#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import glob
import logging_local
import logging_local.config
import mfg_exception
import re
import traceback
import MFT512
import shutil
import toolslib

'''Author: Y.J. Wang @2019.03.22
Descrtiption:
     check dir/file is exist or not
     if dir is not exist: create one
     if file is not exist: create a templete'''

logging_local.config.fileConfig('logging.ini')

logger = logging_local.getLogger(__name__)

mfgerror = mfg_exception.mfgerror

execfile('config.ini')

@MFT512.time_logger
def iqmac_templete(parent, log, mac, mac_rule, path = os.getcwd(), mac_fname = 'demo.text'):
    ''' the templete is follow up the intel IQLite, 
        env. path: {iqlite}/Bin_win/MAC_SERIAL_FILES/IQ_MAC.txt'''
    
    templete = '''
//******************************************************************************
//                  MAC address control file for IQ_UI.DLL                      
//******************************************************************************
// Note: MAC address is 48-bit = (24-bit vendor ID) + (24-bit serial no.)       
//******************************************************************************
//                                                                              
//       Copyright (c) 2006 LitePoint Corp.,   All Rights Reserved              
//                                                                              
//******************************************************************************

IQ_VENDOR_ID   = 0x000000       // Vendor ID
IQ_USED_SERIAL = 0x000000       // Next MAC serial no. to be used
IQ_LAST_SERIAL = 0x000000       // Last MAC serial no.'''
    
    def path_check(path):
        logger.info(path)
        try:
            if not os.path.exists(path):
                os.makedirs(path)
                msg = '{} is created'.format(path)
                return (True, msg)                
            else:
                msg = '{} is exist'.format(path)
                return (True, msg)
        except mfgerror as err:
            logger.error(err.value, exc_info=True)        
            return (False, err.value)           
        except:
            #traceback.print_exc()
            logger.error(traceback.format_exc(), exc_info=True)        
            return (False, traceback.format_exc())

    def file_check(path, mac_fname):
        logger.info(path)
        logger.info(mac_fname)
        try:
            if not os.path.isfile(path + '\\' + mac_fname):
                mac_file = open(path + '\\{}'.format(mac_fname), 'w+')
                mac_file.write(templete)
                mac_file.close()
                msg = '{} is created'.format(mac_fname)
                return (True, msg)
            else:
                msg = '{} is exist'.format(mac_fname)
                return (True, msg)
        except mfgerror as err:
            logger.error(err.value, exc_info=True)
            return (False, err.value)
        except:
            logger.error(traceback.format_exc(), exc_info=True)        
            return (False, traceback.format_exc())

    def mac_write(*args):
        
        def mac_verify(path, mac_fname, vendor, used, last):
            try:
                double_read = open(path + '\\{}'.format(mac_fname), 'r+').read()
                logger.info(double_read)
                wrote_VENDOR = re.search(r'(?<=ID   = )(.+?(\s*))(?=/)', double_read, re.M | re.I).group(1)
                wrote_USED = re.search(r'(?<=USED_SERIAL = )(.+?(\s*))(?=/)', double_read, re.M | re.I).group(1)
                wrote_LAST = re.search(r'(?<=LAST_SERIAL = )(.+?(\s*))(?=/)', double_read, re.M | re.I).group(1)
                logger.debug(wrote_VENDOR)
                logger.debug(wrote_USED)
                logger.debug(wrote_LAST)
                if wrote_VENDOR.strip() != vendor.strip():
                    raise mfgerror('IQ_VENDOR_ID wrote fail')
                if wrote_USED.strip() != used.strip():
                    raise mfgerror('IQ_USED_SERIAL wrote fail')
                if wrote_LAST.strip() != last:
                    raise mfgerror('IQ_LAST_SERIAL wrote fail')
                return (True, (wrote_VENDOR, wrote_USED, wrote_LAST))
            except mfgerror as err:
                logger.error(err.value, exc_info=True)
                return (False, err.value)
            except:
                logger.error(traceback.format_exc(), exc_info=True)        
                return (False, traceback.format_exc())
                
        try:
            mac = args[0]
            mac_rule = args[1]
            path = args[2]
            mac_fname = args[3]
            
            #mac = "%012X"%(int(mac,16)+int(mac_rule))
            cheat_mac = "%012X"%(int(mac,16)-1)
            #mac = mac.upper()
            
            IQ_VENDOR_ID = '0x' + mac[:6]
            IQ_USED_SERIAL = '0x' + cheat_mac[6:]
            IQ_LAST_SERIAL = '0x' + mac[6:]
            logger.info(IQ_VENDOR_ID) 
            logger.info(IQ_USED_SERIAL)
            logger.info(IQ_LAST_SERIAL)
            
            mac_read = open(path + '\\{}'.format(mac_fname), 'r+').read()

            regexDic = {'default_VENDOR': [r'(?<=ID   = )(.+?(\s*))(?=/)', IQ_VENDOR_ID],
                        'default_USED': [r'(?<=USED_SERIAL = )(.+?(\s*))(?=/)', IQ_USED_SERIAL],
                        'default_LAST': [r'(?<=LAST_SERIAL = )(.+?(\s*))(?=/)',IQ_LAST_SERIAL]}
            
            for regex in regexDic.keys():
                mac_read = re.sub(regexDic[regex][0], regexDic[regex][1] + '        ', mac_read)
            #logger.debug(mac_read)

            mac_file = open(path + '\\{}'.format(mac_fname), 'w+')
            mac_file.write(mac_read)
            mac_file.close()
            confirm = mac_verify(path, mac_fname, IQ_VENDOR_ID, IQ_USED_SERIAL, IQ_LAST_SERIAL)
            parent.SendMessage('IQ_VENDOR_ID: {}\n'.format(confirm[1][0]), log)
            parent.SendMessage('IQ_USED_SERIAL: {}\n'.format(confirm[1][1]), log)
            parent.SendMessage('IQ_LAST_SERIAL: {}\n'.format(confirm[1][2]), log)
            return confirm
        except mfgerror as err:
            logger.error(err.value, exc_info=True)
            return (False, err.value)
        except:
            logger.error(traceback.format_exc(), exc_info=True)        
            return (False, traceback.format_exc())

    r1 = path_check(path)
    r2 = file_check(path, mac_fname)
    logger.debug(r1)
    logger.debug(r2)
    
    if path_check(path)[0] and file_check(path, mac_fname)[0]:
        result = mac_write(mac, mac_rule, path, mac_fname)
        logger.debug(result)
        return result


@MFT512.time_logger
def iq_port_setup(parent, log, path = os.getcwd(), port = '1', module = '192.168.100.254:A'):
    logger.info(path)
    try:
        if iq_port:
            port = str(int(iq_port[0]) + 1)
            module = IQip + ':' + iq_port[1]
            
        flow_read = open(path, 'r+').read()
        regexDic = {'default_VSA': [r'VSA_PORT.*=(.*)', port],
                    'default_VSG': [r'VSG_PORT.*=(.*)', port],
                    'default_module': [r'IQTESTER_MODULE_01.*=(.*)',module]}
            
        for regex in regexDic.keys():
            pattern = re.compile(regexDic[regex][0], re.M | re.I)
            matches = pattern.search(flow_read)
            if matches is None:
                parsing = None
            else:
                parsing = matches.group(0)
                logger.debug(parsing)
                replace = parsing.split('=')[0] + '= ' + regexDic[regex][1]
                logger.info(replace)
                
                if parsing in flow_read:
                    #flow_read = re.sub(parsing, replace, flow_read)
                    flow_read = flow_read.replace(parsing, replace)
                else:
                    raise mfgerror('err')
                

        flow_file = open(path, 'w+')
        flow_file.write(flow_read)
        flow_file.close()
        flow_update = open(path, 'r+').read()
        logger.debug(flow_update)
        parent.SendMessage('VSA: {}\n'.format(int(port)-1), log)
        parent.SendMessage('VSG: {}\n'.format(int(port)-1), log)    
        parent.SendMessage('Module: {}\n'.format(module), log)
        return (True, flow_update)

    except mfgerror as err:
        logger.error(err.value, exc_info=True)
        return (False, err.value)
    except:
        logger.error(traceback.format_exc(), exc_info=True)        
        return (False, traceback.format_exc())


@MFT512.time_logger
def cal_file_copy(parent, log, term, mac, wlan, path = os.getcwd(), bin_file = 'cal_wlan0.bin'):
    
    def path_check(path):
        logger.info(path)
        try:
            if not os.path.exists(path):
                os.makedirs(path)
                msg = '{} is created'.format(path)
                return (True, msg)                
            else:
                msg = '{} is exist'.format(path)
                return (True, msg)
        except mfgerror as err:
            logger.error(err.value, exc_info=True)        
            return (False, err.value)           
        except:
            #traceback.print_exc()
            logger.error(traceback.format_exc(), exc_info=True)        
            return (False, traceback.format_exc())

    def file_check(mac, wlan, path, bin_file):
        logger.info(path)
        logger.info(wlan)
        logger.info(bin_file)
        logger.info(mac)
        try:
            #mac = "%012X"%(int(mac,16)+int(interface[wlan][2]))
            if wlan == '2G':
                magic_code = ['2442 MCS7', mac]
            else:
                magic_code = ['5180 MCS7', mac]

            list_of_files = glob.glob(path + '/**/*.bin')
            latest_cal_file = max(list_of_files, key=os.path.getctime)
            latest_folder = latest_cal_file.split(bin_file)[0].strip()
            logger.info(latest_cal_file)
            logger.info(latest_folder)
            callog = open(latest_folder + 'detailed_log.txt','r').read()
            
            for match_str in magic_code:
                if match_str not in callog:
                    raise mfgerror('Fail: {}-{} cal folder not found'.format(wlan, mac))
            
            if not os.path.isfile(latest_cal_file):
                raise mfgerror('Fail: {} cal file not found'.format(wlan))
            else:
                host_md5 = os.popen('md5sums {}'.format(latest_cal_file)).read()
                org_md5 = host_md5.split(bin_file)[-1].strip()
                logger.info(host_md5)
                logger.info(org_md5)
                msg = '{} md5: {}'.format(interface[wlan][-1], org_md5)
                parent.SendMessage(msg + '\n')
                return (True, org_md5, latest_folder, msg)
        
        except mfgerror as err:
            logger.error(err.value, exc_info=True)
            return (False, err.value)
        except:
            logger.error(traceback.format_exc(), exc_info=True)        
            return (False, traceback.format_exc())

    def file_copy(mac, wlan, md5, path, bin_file):
        logger.debug(path)
        try:
            #mac = "%012X"%(int(mac,16)+int(interface[wlan][2]))
            src = path + bin_file
            new_path = path.rsplit('\\', 2)[0] + '\\{}'.format(mac)
            if os.path.exists(new_path):
                shutil.rmtree(new_path, ignore_errors=True)
            dst = TmpCalFilePath + '\\' + interface[wlan][5]
            shutil.copy2(src, dst)
            logger.info(path)
            logger.info(new_path)
            os.rename(path, new_path)
            file_md5 = os.popen('md5sums {}'.format(dst)).read()
            cp_md5 = file_md5.split(interface[wlan][5])[-1].strip()
            logger.info(cp_md5)
            if cp_md5 == md5:
                msg = '{} copy to {} success'.format(interface[wlan][5], TmpCalFilePath)
                return (True, msg)
            else:
                raise mfgerror('Fail: md5sums not matched')

        except mfgerror as err:
            logger.error(err.value, exc_info=True)
            return (False, err.value)
        except:
            logger.error(traceback.format_exc(), exc_info=True)        
            return (False, traceback.format_exc())

    def file_tftp(term, wlan, md5):
        try:
            toolslib.lWaitCmdTerm(term,'cd {}'.format(BoardCalFilePath), BoardCalFilePath,5)
            toolslib.lWaitCmdTerm(term,'tftp -g {} -r {}'.format(tftp_server, interface[wlan][5]),"#",5)
            toolslib.lWaitCmdTerm(term,'sync',"#",5)
            for retry in range(3):
                term.get()
                board_md5 = toolslib.lWaitCmdTerm(term,'md5sum {}'.format(BoardCalFilePath+interface[wlan][5]),"#",5)
                parent.SendMessage(board_md5 + '\n')
                if md5 in board_md5:
                    msg = '{} copy to {} via tftp success'.format(interface[wlan][5], BoardCalFilePath)
                    parent.SendMessage(msg + '\n')
                    return (True, msg)
                else:
                    if retry == 2:
                        raise mfgerror('Fail: md5sums not matched')
                
        except mfgerror as err:
            logger.error(err.value, exc_info=True)
            return (False, err.value)
        except:
            logger.error(traceback.format_exc(), exc_info=True)        
            return (False, traceback.format_exc())


    r1 = path_check(TmpCalFilePath)
    if r1[0]:
        r2 = file_check(mac, wlan, HostCalFilePath, bin_file)
        if r2[0]:
            r3 = file_copy(mac, wlan, r2[1], r2[2], bin_file)
            if r3[0]:
                r4 = file_tftp(term, wlan, r2[1])
                return r4
            else:
                return r3
        else: 
            return r2
    else:
        return r1

def main():
    iqmac_templete('123456789012', interface['5G'][2], os.getcwd() + '\\test')
    cal_file_copy('2G')
if __name__ == '__main__':
    main()
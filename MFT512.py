import os
import sys
import time
import traceback
from toolslib import * 
import socket,re
import bz2
import odbc
import glob
import logging_local
import logging_local.config
import mfg_exception
from functools import wraps

logging_local.config.fileConfig('logging.ini')

logger = logging_local.getLogger(__name__)

mfgerror = mfg_exception.mfgerror

execfile("config.ini")
if os.path.isfile('c:\\station.ini'):
    execfile('c:\\station.ini')


def time_logger(func):
    @wraps(func)
    def timing(*args):
        args[0].SendMessage('\n[ {} ]\n'.format(func.__name__), args[1])
        test_time = time.time()
        result = func(*args)
        if result[0]:
        	args[0].SendMessage('\n[ {} ] Pass'.format(func.__name__), args[1], color=2)
        else:
        	args[0].SendMessage('\n[ {} ] Fail\n'.format(func.__name__), args[1], color=1)
        args[0].SendMessage('\n------------------ Test time: {:03.2f} (sec) ------------------\n'.format(float(time.time()-test_time)), args[1], color=0)
        return result
    return timing


def find(pattern, string):
    match = re.search(pattern,string)
    if match: return match.group()
    else: raise Except("re string not find:%s"%pattern)

@time_logger    
def IsConnect(parent, log, ip, timeout):
    try:
        ip = ip.strip()
        os.popen("arp -d")
        start_time = time.time()
        while time.time() - start_time <= timeout:            
            data = int(os.popen("ping -w 1000 -n 3 %s"%ip).read().split('(')[-1].split('%')[0])
            if not data:
                parent.SendMessage('{} ping success\n'.format(ip), log, color=0)
                return (True, data)
            else:
                continue
        raise mfgerror('FAIL: {} ping fail'.format(ip))
    except mfgerror as err:
        logger.error(err.value, exc_info=True)
        return (False, err.value)
    except:
        logger.error(traceback.format_exc(), exc_info=True)        
        return (False, traceback.format_exc())

 
def GetMacAddress(parent):
    val = parent.MAC.GetValue()
    try:
        if len(val) == 12 and int(val,16):
           return val
    except ValueError:
           pass
    raise Except("Input Label Error %s !"%val)


@time_logger
def USBTest(parent, log, term):
    parent.SendMessage('\n'+"USB Test...............\n",log, color=0)
    for i in range(3):
        data = lWaitCmdTerm(term,"mount","#",5,3)
        if not '/dev/sda1' in data or not "/dev/sdb1" in data:
           time.sleep(1)
           if i ==2:raise Except("USB Mount FAIL.")
        else:
           addr1 = data.split("/dev/sda1 on")[1].strip().split()[0].strip()
           addr2 = data.split("/dev/sdb1 on")[1].strip().split()[0].strip()  
           break     
       #parent.SendMessage("%s"%addr,log,color=2)
    term.get()
    data1 = lWaitCmdTerm(term,"ls %s"%addr1,"#",5,3)
    data2 = lWaitCmdTerm(term,"ls %s"%addr2,"#",5,3)
    
    if not 'test.txt' in data1 or not 'test.txt' in data2:
        parent.SendMessage('\n'+"check content FAIL\n",log,color=1)
        raise Except("Check USB Content FAIL.")
    else:
        parent.SendMessage('\n'+"check content PASS\n",log,color=2)


def getSelfAddr():
    ip = socket.gethostbyname_ex(socket.gethostname())
    for i in ip[-1]:
       if '172.28.' in i:
          return i
    return 0


@time_logger
def Chec_ver(parent, log, term):
    for i in range(3):
        data = lWaitCmdTerm(term,"cat /proc/athversion",'#',10,2) 
        parent.SendMessage("\n%s\n"%data,log,color=2)
        if '10.2-00082-4' not in data:
           if i ==2:raise Except("failed: %s"%data)
        else:break

@time_logger
def CheckWifiInterface(parent, log, term, cmd, wait_cmd, time_out): 
    try:
        retry = 0
        test_time = time.time()
        while (time.time()- test_time) <= time_out: 
              term.get()
              term << cmd
              data = term.wait('#',2)[-1]
              if wait_cmd in data:
                  logger.info(data)
                  parent.SendMessage(data + '\n', log, color=0)
                  return (True, data)    
              else:
                  logger.info('check wifi interface retry: {}'.format(retry))
                  time.sleep(3)
                  retry += 1
        raise mfgerror('FAIL: Interface not bringup ({})'.format(cmd))          
    except mfgerror as err:
        logger.error(err.value, exc_info=True)
        return (False, err.value)
    
    except:
        logger.error(traceback.format_exc(), exc_info=True)
        return (False, traceback.format_exc())    

@time_logger
def Intel_cal_server_bringup(parent, log, term):
    try:
        counting = 0
        result = None
        def Intel_cal_server_checkout(parent, log, term):
            ps = str()
            process_exist = None
            process_keypoint = {'dutserver':'/usr/sbin/dutserver'}
            for process in process_keypoint.keys():
                ps += lWaitCmdTerm(term, 'ps | grep {}'.format(process), "#", 5)            
                if process_keypoint[process] in ps:
                    process_exist = True
                else:
                    process_exist = False
            parent.SendMessage(ps + '\n', log, color=0)
            return (process_exist, ps)
            
        result = Intel_cal_server_checkout(parent, log, term)
        if not result[0]:    
            lWaitCmdTerm(term,'echo 6 cdebug=-1 > /proc/net/mtlk_log/debug',"#",5)
            lWaitCmdTerm(term,'/usr/sbin/dutserver &',"Waiting for connections",5)
            time.sleep(2)
            result = Intel_cal_server_checkout(parent, log, term)
            logger.debug(result)
            if not result[0]:
                raise mfgerror('FAIL: {}'.format(sys._getframe().f_code.co_name))
            else:
                return result
        else:
            return result
        
    except mfgerror as err:
        logger.error(err.value, exc_info=True)
        return (False, err.value)
    
    except:
        logger.error(traceback.format_exc(), exc_info=True)
        return (False, traceback.format_exc())

import os,time,traceback
from toolslib import *
from MFT512 import *
import socket,re
import bz2
import odbc
import glob
import logging_local
import logging_local.config
import path_check
import mfg_exception
from subprocess_stdout import *



logging_local.config.fileConfig('logging.ini')

logger = logging_local.getLogger(__name__)

execfile("config.ini")
if os.path.isfile('c:\\station.ini'):
    execfile('c:\\station.ini')


###########   Main script    ###########     
def W11(parent):
    for i in range(1):
        try:
            result = None
            log = None
            mac = str()
            parent.SendMessage("START",state = "START")
            start_time = end_time = 0        
            #term =htx.SerialTTY(comport,115200)    
            mac = GetMacAddress(parent)
            mac = str(mac).upper()
            
            if pwr_chk:
                station = 'pck'
            else:
                station = 'w11'

            if i > 0:     
                log = open(logPath+mac+"_{}.{}".format(i, station),"w")
            else: 
                log = open(logPath+mac+".{}".format(station),"w")
     
            #MES inspection 
            #checktravel(mac,'127.0.0.1',1800,20)
    
            start_time=time.time()
            parent.SendMessage("WIFI Calibration Tool Version: %s , Station: %s (%s)\n"%(version,Test_position, parent.type),log, color=0)
            parent.SendMessage( "---------------------------------------\n",log, color=0)
            parent.SendMessage( "Chipset: " + ICType + "\n", log, color=0)
            parent.SendMessage( "Start Time: " + time.ctime() + "\n", log, color=0)
            parent.SendMessage( "Scan MAC address: " + mac + "\n", log, color=0)
            
            result = IsConnect(parent, log, IQip, 5)
            if not result[0]:
                raise mfgerror(result[1])
            result = IsConnect(parent, log, dut_ip, 60)
            if not result[0]:
                raise mfgerror(result[1])
            
            term = htx.SerialTTY(comport,b_rate)
                
            for item in interface.keys():
                parent.SendMessage( '\n[ {} testmode start ]\n'.format(item), log, color=0)
                wifi_mac = "%012X"%(int(mac,16)+int(interface[item][2]))
                wifi_mac = str(wifi_mac).upper()
                
                if pwr_chk:
                    bat = interface[item][1]
                else:
                    bat = interface[item][0]

                content = open(bat).read()
                flow = content.split('-RUN')[-1].split('-exit')[0].strip()
                parent.Flow.SetLabel(os.getcwd() + '\\{}'.format(flow))

                result = path_check.iq_port_setup(parent, log, os.getcwd() + '\\iqlite\\{}'.format(flow))
                if not result[0]:
                    raise mfgerror(result[1])           
                
                result = path_check.iqmac_templete(parent, log, wifi_mac, interface[item][2], mac_path, mac_fname)                
                if not result[0]:
                    raise mfgerror(result[1])           
                
                result = CheckWifiInterface(parent, log, term, 'iwconfig {}'.format(interface[item][3]),'802.{}'.format(interface[item][4]), 30)           
                if not result[0]:
                    raise mfgerror(result[1])           
                
                result = Intel_cal_server_bringup(parent, log, term)
                if not result[0]:
                    raise mfgerror(result[1])           
                
                output = str()
                wait_count = 0
                queue = Queue.Queue()

                process = subprocess.Popen(bat,
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.STDOUT,
                                           bufsize=-1,
                                           shell=True
                                           )
                
                task = threading.Thread(target=task_read_stdout, args=(process, queue))
                task.daemon = True
                task.start()
                while process.poll() is None or not queue.empty():
                    try:
                        time.sleep(0.05)
                        output = queue.get(timeout=1)
                        parent.SendMessage(output, log, color=0)

                    except Queue.Empty:
                        wait_count += 1
                        if wait_count > 500:
                            process.terminate()
                            process.wait()
                            logger.info('queue is empty: ' + str(wait_count))
                            raise mfgerror('FAIL: ' + 'process hang')    
                        time.sleep(0.1)
                    if not output:
                        logger.info('no output')
                        continue
                    else:
                        wait_count = 0
                        
                task.join()
                
                if not os.path.isfile(os.getcwd() + '\\iqlite\\Log\\detailed_log.txt'): 
                    raise mfgerror("FAIL: IQlite result file not found")
                
                f = open(os.getcwd() + '\\iqlite\\Log\\detailed_log.txt','r').read()                
                if not pwr_chk:
                    if wifi_mac not in f: 
                        raise mfgerror("FAIL: {} MFG process fail".format(item))
                
                if '*  P A S S  *' not in f: 
                    raise mfgerror("FAIL: {} fail".format(station))

                if not pwr_chk:
                    result = path_check.cal_file_copy(parent, log, term, wifi_mac, item)
                    if not result[0]:
                        raise mfgerror(result[1])
                
                os.popen("del %s%s"%(os.getcwd(), '\\iqlite\\Log\\*.txt')).read()

            result = result[0]
        except Except,msg:
            parent.SendMessage("\n%s\n"%msg, log, color=1)
            result = False
        except mfgerror as err:
            logger.error(err.value, exc_info=True)
            parent.SendMessage('\n' + err.value + '\n', log, color=1)
            result = False
        except: 
            parent.SendMessage(traceback.format_exc(), log, color=1)
            result = False
        #parent.tcps.set('unload')
        end_time = time.time()
        parent.SendMessage('\n'+"End Time:"+time.ctime()+'\n',log, color=0)
        parent.SendMessage("total time: %3.2f"%(end_time-start_time)+'\n',log, color=0)
        
        if not result:
           parent.SendMessage( "Test Result:FAIL"+'\n',log, color=1)
           parent.SendMessage("",state = "FAIL")       
        else:
           parent.SendMessage( "Test Result:PASS"+'\n',log, color=2)
           parent.SendMessage("",state = "PASS")
        
        if log:log.close()
        
        if not result:
            if i > 0: os.system("rename %s%s %s"%(logPath,mac+"_%d.w11"%i,mac+"_%d_fail.w11"%i))
    
def W21(parent):
    pass

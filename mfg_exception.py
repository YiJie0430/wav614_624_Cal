#!/usr/bin/python
# -*- coding: utf-8 -*-

import traceback

'''Author: Y.J. Wang @2019.03.22
Descrtiption:
	 MFG exception setup here'''


class mfgerror:
	def __init__(self, value):
		self.value = value

	def __str__(self):
		return self.value
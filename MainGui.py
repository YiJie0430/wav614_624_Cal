# -*- coding:BIG5-*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 30 2011)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx,cStringIO,os,sys,odbc
import wx.xrc,thread
from TCPService import TCPService
from tftp import tftpcfg, tftp_engine
import htx
sys.path.append(os.getcwd())
from MFT512 import *
from path_check import *
from MainScript import *
import logging_local
import logging_local.config

logging_local.config.fileConfig('logging.ini', disable_existing_loggers=False)

logger = logging_local.getLogger(__name__)

log_lock = thread.allocate_lock()

logger.debug('[debug mode]')

###########################################################################
## Class MyFrame1
###########################################################################
execfile("config.ini")
if os.path.isfile('c:\\station.ini'):
   execfile('c:\\station.ini')


def GetTargetPower():
    f = open(TargetPowerFilePath,'r')
    value = f.read()
    f.close()
    value = value.split("),f.0;")[0].split(',')[-1]
    return value
    
def GetCableLoss():
    import glob   
    global CableLossPath   
    if CableLossPath[-1] not in ["/","\\"]: CableLossPath += "/"
    fs = glob.glob(CableLossPath+'Pathloss_*.csv')
    if not fs:
       htx.Win32Message("Warning!","No such cable loss file.")
       sys.exit(1)
    """ 
    loss = open(fs[-1],'r').read()
    f = open("bin/start_default.art",'r')
    val = '''#----------------------------------------------------------------
#Pathlosses 
#----------------------------------------------------------------\n'''+loss+f.read()+"\nequipment model=litepoint; arg=%s	# Litepoint"%IQip
    f.close
    f = open("bin/start.art",'w') 
    f.write(val)
    f.close()  
    f = open("bin/litepoint_setup_default.txt",'r')
    val ="RFPORT=%s;			# 0: left RFport(default), 1: right RFport"%IQPort+f.read()  
    f.close()
    f = open("bin/litepoint_setup.txt",'w') 
    f.write(val)
    f.close()   
    """ 
    return os.path.abspath(fs[-1])

class AtherosFrame ( wx.Frame ):
      def __init__( self ):
          wx.Frame.__init__ ( self, None, 
                              id = wx.ID_ANY, 
                              title = '%s  |  Version: %s'%(StationCaption,version), 
                              pos = wx.DefaultPosition, size = wx.Size( 790,700 ), 
                              style =  wx.SYSTEM_MENU|wx.CAPTION|wx.MINIMIZE_BOX|wx.CLOSE_BOX|wx.TAB_TRAVERSAL|wx.BORDER_DEFAULT )
          
          self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
          self.SetBackgroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_MENUBAR))
          IconStream='\x00\x00\x01\x00\x01\x00  \x10\x00\x00\x00\x00\x00\xe8\x02\x00\x00\x16\x00\x00\x00(\x00\x00\x00 \x00\x00\x00@\x00\x00\x00\x01\x00\x04\x00\x00\x00\x00\x00\x80\x02\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x80\x00\x00\x80\x00\x00\x00\x80\x80\x00\x80\x00\x00\x00\x80\x00\x80\x00\x80\x80\x00\x00\x80\x80\x80\x00\xc0\xc0\xc0\x00\x00\x00\xff\x00\x00\xff\x00\x00\x00\xff\xff\x00\xff\x00\x00\x00\xff\x00\xff\x00\xff\xff\x00\x00\xff\xff\xff\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\n\xaa\xa0\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xe1\x86\x18a\xff\x86\x18a\xff\x86\x18a\xff\x86\x18a\xff\x86\x18a\xff\x86\x18a\xff\xfe\x18a\xff\xfe\x18a\xff\xfe\x18a\xff\xfe\x18a\xff\xff\xf8a\xff\xff\xf8a\xff\xff\xf8a\xff\xff\xf8a\xff\xff\xf8a\xff\xff\xff\xe1\xff\xff\xff\xe1\xff\xff\xff\xe1\xff\xff\xff\xe1'
          stream = cStringIO.StringIO(IconStream)
          icon = wx.EmptyIcon()
          icon.CopyFromBitmap(wx.BitmapFromImage(wx.ImageFromStream(stream)))
          self.SetIcon(icon) 
          bSizer1 = wx.BoxSizer( wx.VERTICAL )
          bSizer2 = wx.BoxSizer( wx.HORIZONTAL )
          self.m_bitmap3 = wx.StaticBitmap( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 10 )
          img=wx.Image('images/Hitron_logo.png')
          self.m_bitmap3.SetBitmap( wx.BitmapFromImage(img))
          bSizer2.Add( self.m_bitmap3, 0, wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT, 5)
          '''
          ###########capiton###########
          self.Caption = wx.StaticText( self, wx.ID_ANY, StationCaption , wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
          self.Caption.Wrap( -1 )
          self.Caption.SetFont( wx.Font( 18, 74, 90, 92, False, "Tahoma" ) )
          bSizer2.Add( self.Caption, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
          '''
          bSizer1.Add( bSizer2, 0, wx.EXPAND|wx.ALL|wx.ALIGN_RIGHT, 15 )
          ###########################
          #self.m_staticline1 = wx.StaticLine( self, -1, wx.DefaultPosition, wx.DefaultSize)
          #bSizer1.Add( self.m_staticline1, 0, wx.EXPAND|wx.ALL, 0 )
          bSizer3 = wx.BoxSizer( wx.VERTICAL )
          bSizer4 = wx.BoxSizer( wx.HORIZONTAL )
          ############station##############
          self.m_staticText4 = wx.StaticText( self, wx.ID_ANY, u"Station:  ", wx.DefaultPosition, wx.DefaultSize, 0 )
          self.m_staticText4.Wrap( -1 )
          self.m_staticText4.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD))
          bSizer4.Add( self.m_staticText4, 0, wx.ALL, 5 )
          if pwr_chk:
              self.type = 'Power Check'
          else:
              self.type = 'WIFI Cal.'
          self.station = wx.StaticText( self, wx.ID_ANY, u"%s (%s)"%(Test_position, self.type), wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
          self.station.Wrap( -1 )
          self.station.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
          bSizer4.Add( self.station, 0, wx.ALL, 5 )
          bSizer3.Add( bSizer4, 0, wx.EXPAND, 5 )
          bSizer41 = wx.BoxSizer( wx.HORIZONTAL )
          ###############IC Type###############
          self.m_staticText41 = wx.StaticText( self, wx.ID_ANY, u"IC Type: ", wx.DefaultPosition, wx.DefaultSize, 0 )
          self.m_staticText41.Wrap( -1 )
          self.m_staticText41.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD))
          bSizer41.Add( self.m_staticText41, 0, wx.ALL, 5 )
          self.ICType = wx.StaticText( self, wx.ID_ANY, u"%s"%ICType, wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
          self.ICType.Wrap( -1 )
          self.ICType.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
          bSizer41.Add( self.ICType, 0, wx.ALL, 5 )
          bSizer3.Add( bSizer41, 0, wx.EXPAND, 5 )
          bSizer411 = wx.BoxSizer( wx.HORIZONTAL )
          ###############IQ Port###############
          self.m_staticText411 = wx.StaticText( self, wx.ID_ANY, u"IQxel-M8W Port:  ", wx.DefaultPosition, wx.DefaultSize, 0 )
          self.m_staticText411.Wrap( -1 )
          self.m_staticText411.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD))
          bSizer411.Add( self.m_staticText411, 0, wx.ALL, 5 )
          self.IQPort = wx.StaticText( self, wx.ID_ANY, u"%s"%iq_port, wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
          self.IQPort.Wrap( -1 )
          self.IQPort.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
          bSizer411.Add( self.IQPort, 0, wx.ALL, 5 )
          bSizer3.Add( bSizer411, 0, wx.EXPAND, 5 )
          bSizer4111 = wx.BoxSizer( wx.HORIZONTAL )
          
          ###############Target Power###############
          self.m_staticText4111 = wx.StaticText( self, wx.ID_ANY, u"Test Flow: ", wx.DefaultPosition, wx.DefaultSize, 0 )
          self.m_staticText4111.Wrap( -1 )
          self.m_staticText4111.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD))
          bSizer4111.Add( self.m_staticText4111, 0, wx.ALL, 5 )
          
          self.Flow = wx.StaticText( self, wx.ID_ANY, u"INIT. ", wx.DefaultPosition, wx.DefaultSize, 0 )
          self.Flow.Wrap( -1 )
          self.Flow.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
          bSizer4111.Add( self.Flow, 0, wx.ALL, 5 )
          bSizer3.Add( bSizer4111, 0, wx.EXPAND, 5 )
          
          bSizer41111 = wx.BoxSizer( wx.HORIZONTAL )
          ###############Cable loss file###############
          self.m_staticText41111 = wx.StaticText( self, wx.ID_ANY, u"Cable Loss File:", wx.DefaultPosition, wx.DefaultSize, 0 )
          self.m_staticText41111.Wrap( -1 )
          self.m_staticText41111.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD))
          bSizer41111.Add( self.m_staticText41111, 0, wx.ALL, 5 )
          self.CableLossFile = wx.StaticText( self, wx.ID_ANY, u"%s"%CableLossPath, wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_CENTRE )
          self.CableLossFile.Wrap( -1 )
          self.CableLossFile.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
          bSizer41111.Add( self.CableLossFile, 0, wx.ALL, 5 )
          bSizer3.Add( bSizer41111, 0, wx.EXPAND, 5 )
          #self.m_staticline3 = wx.StaticLine( self, -1, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
          #bSizer3.Add( self.m_staticline3, 0, wx.EXPAND |wx.ALL, 5 )
          bSizer1.Add( bSizer3, 0, wx.EXPAND|wx.ALL, 5 )
          bSizer20 = wx.BoxSizer( wx.VERTICAL )
          bSizer41112 = wx.BoxSizer( wx.HORIZONTAL )
          ############### Input Mac ###############
          self.m_staticText41112 = wx.StaticText( self, wx.ID_ANY, u"MAC:  ", wx.DefaultPosition, wx.DefaultSize, 0 )
          self.m_staticText41112.Wrap( -1 )
          self.m_staticText41112.SetFont(wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.BOLD))
          bSizer41112.Add( self.m_staticText41112, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
          self.MAC = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(120, -1), wx.TE_PROCESS_ENTER  )
          self.MAC.SetFont( wx.Font(10, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
          self.MAC.SetBackgroundColour( wx.Colour( 255, 255, 153 ) )   
          bSizer41112.Add( self.MAC, 0, wx.ALL, 5 )
          self.StartBtn = wx.Button(self, wx.ID_OK, "START")
          bSizer41112.Add( self.StartBtn, 0, wx.EXPAND|wx.ALL|wx.ST_NO_AUTORESIZE, 5 ) 
          bSizer41112.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 ) 
          ############### Result ###############         
          self.Result = wx.StaticText( self, wx.ID_ANY, u"INIT.", wx.DefaultPosition, wx.Size(250, -1), wx.ALIGN_CENTRE_HORIZONTAL|wx.ST_NO_AUTORESIZE )
          self.Result.Wrap(-1)
          self.Result.SetFont( wx.Font( 15, wx.DEFAULT, wx.NORMAL, wx.BOLD ) )
          self.Result.SetBackgroundColour( wx.Colour( 192, 192, 192 ) )          
          bSizer41112.Add( self.Result, 0, wx.EXPAND|wx.ALL|wx.ALIGN_CENTRE, 5 )          
          bSizer41112.AddSpacer( (0, 0), 1, wx.EXPAND|wx.ALL, 5 )
          bSizer20.Add( bSizer41112, 1, wx.EXPAND|wx.RIGHT|wx.ALIGN_CENTER, 5 )  
          self.m_staticline4 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
          bSizer20.Add( self.m_staticline4, 0, wx.EXPAND |wx.ALL, 5 )     
          bSizer1.Add( bSizer20, 0, wx.EXPAND|wx.ALL, 5 )      
          ############### Log ###############   
          self.Log = wx.TextCtrl( self, wx.ID_ANY, u'', wx.DefaultPosition, wx.DefaultSize, wx.TE_PROCESS_TAB|wx.TE_MULTILINE
                                                                                              |wx.HSCROLL|wx.TE_RICH2|wx.TE_READONLY
                                                                                              |wx.TE_LINEWRAP|wx.HSCROLL|wx.VSCROLL )
          self.Log.SetFont( wx.Font( 10, wx.DEFAULT, wx.NORMAL, wx.LIGHT) )
          #self.Log.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
          self.Log.SetBackgroundColour( wx.Colour( 32, 32, 32 ) )          
          bSizer1.Add( self.Log, 1, wx.ALL|wx.EXPAND, 8 )          
          
          self.SetSizer( bSizer1 )
          self.Layout()          
          self.Centre( wx.BOTH ) 
          self.buf = ''
          self.run_cart=False
          self.running = True
          self.timer_ = wx.Timer(self)
          self.Bind(wx.EVT_TIMER, self.MACSetFocus, self.timer_)
          self.timer_.Start(1000)
          self.MAC.Bind( wx.EVT_TEXT_ENTER, self.OnFocusStart )
          self.Bind( wx.EVT_CLOSE, self.close )
          self.Bind(wx.EVT_BUTTON, self.scan, self.StartBtn)
          self.en_ = 0
          #self.MAC.SetValue("F81D0F28E1C0")
          ##########inint tcp and tftp service############
          #os.popen("TASKKILL /F /IM cart.exe /T")
          #self.tcps = TCPService(self)
          #self.tcps.start()
          
          if not os.path.exists(TmpCalFilePath):
              os.makedirs(TmpCalFilePath)
          self.tcps = TCPService(self)
          self.tcps.start()
          f = open("tftp.ini",'r') 
          tftp_cfg = f.read() 
          f.close()
          f=open("tftp.ini",'w')
          for i in tftp_cfg.splitlines(): 
              if 'tftprootfolder' in i: i = "tftprootfolder = %s"%TmpCalFilePath
              f.write(i + '\n')    
          f.close()
          cfgdict = tftpcfg.getconfigstrict(os.getcwd, 'tftp.ini')
          self.TFTPServer = tftp_engine.ServerState(**cfgdict)
          thread.start_new_thread(tftp_engine.loop_nogui, (self.TFTPServer,))
          logger.info(self.TFTPServer)
          
          
      
      def OnFocusStart(self,event):
          self.StartBtn.SetFocus()
    
      def ShowResult(self,val):
          color = {"PASS":wx.Colour( 102, 255, 102 ),
                   "FAIL":wx.Colour( 255, 88, 88 ),
                   "START":wx.Colour( 255, 255, 102 )
                  }
          self.Result.SetBackgroundColour(color[val])  
          if val=="START":
             self.Log.SetValue('')
             val="Running" 
             self.MAC.Enable(False) 
          else:
             self.MAC.Enable(True)
             time.sleep(0.1)
             self.en_ = 1
             self.StartBtn.Enable(True) 
          self.Result.SetLabel("%s"%val)
          
      def SendMessage(self,val,log=None,state="",color=0):
          colors=[(220,220,220),(255,102,102),(102,255,102)]          
          log_lock.acquire()
          beg = self.Log.GetLastPosition()
          end = self.Log.GetLastPosition() + len(val)
          self.Log.SetStyle(beg,end,wx.TextAttr(colors[color]))
          self.Log.AppendText(val)
          self.Log.ShowPosition(end)
          log_lock.release()          
          
          if log:
              log.write(val)
          
          if state in ("PASS","FAIL","START"):
              self.ShowResult(state)
          

      def MACSetFocus(self,evt):
          if self.MAC.Enabled and self.en_:
             self.MAC.SetFocus()
             self.MAC.SetSelection(-1,-1)
             self.en_ = 0
          
      def scan(self,evt):
          self.StartBtn.Enable(False) 
          #if not self.run_cart:
          #   os.startfile('ni.bat')
          #   #self.run_cart = True
          thread.start_new_thread(eval(FunctionName),(self,))
          
      def close( self,evt ):
          try:
              self.TFTPServer.shutdown()
              if self.running:
                 self.running = False
                 #self.tcps.close()
          except:
              pass
          sys.exit(1)

class App(wx.App):
    def OnInit(self):
        try:
            self.main = AtherosFrame()
            self.main.Show(True)
            self.SetTopWindow(self.main)
        except Exception,e:
            print e
        return True

if __name__ == '__main__':
   application = App(0)
   application.MainLoop()


@ECHO OFF

IF EXIST "C:\Program Files (x86)\Intel\DUT\developer" (
  SET Dutpth="C:\Program Files (x86)\Intel\DUT\developer\dut.dll"
  SET linuxHostspth="C:\Program Files (x86)\Intel\DUT\autoloader\linux_hosts.txt"
) ELSE IF EXIST "C:\Program Files\Intel\DUT\developer" (
  SET Dutpth="C:\Program Files\Intel\DUT\developer\dut.dll"
  SET linuxHostspth="C:\Program Files\Intel\DUT\autoloader\linux_hosts.txt"
)

XCOPY %Dutpth% "%CD%" /y /f /e
XCOPY %linuxHostspth% "%CD%\autoloader" /y /f /e
================================= Run 1 ===================================

Summary
1.CONNECT_IQ_TESTER __________________________________________________________
Test Time = 0.340 s
ERROR_MESSAGE            :[Info] Function completed.
      
IQAPI_VERSION            :                               
IQLITE_VERSION           :WAV500 & WAV600 6024 (2018-10-16)      
IQMEASURE_VERSION        :3.1.3 (2016-12-08) 205784      
IQTESTER_INFO            :IQmeasure: 3.1.3 (2016-12-08) 205784
Tester 1 SN: IQ111EA0378
Model number: IQXEL-M16I
Firmware revision: 1.14.0
Tester 1 hardware version: M.5.1.0

      
IQTESTER_SERIAL_NUM_01   :IQ111EA0378                    
IQTESTER_SERIAL_NUM_02   :                               
IQTESTER_SERIAL_NUM_03   :                               
IQTESTER_SERIAL_NUM_04   :                               
IQV_ANALYSIS_VERSION     :                               
IQV_MW_VERSION           :                               
IQ_MAX_SIGNAL_VERSION    :                               
IQ_NXN_SIGNAL_VERSION    :                               
IQ_NXN_VERSION           :                               
TEST_MANAGER_VERSION     :3.0.0 (2012-8-24)              
2.DEFAULT_GLOBAL_SETTINGS ____________________________________________________
Test Time = 0.000 s
3.GLOBAL_SETTINGS ____________________________________________________________
Test Time = 0.000 s
4.LOAD_PATH_LOSS_TABLE _______________________________________________________
Test Time = 0.000 s
ERROR_MESSAGE            :[Info] Function completed.
      
5.INSERT_DUT _________________________________________________________________
Test Time = 0.616 s
NLOG                     :           0                    (, )
REGION_1                 :          62                    (, )
REGION_2                 :          62                    (, )
REGION_3                 :          62                    (, )
RX_CAL_NUM_OF_LNA_GAINS  :           6                    (, )
DUT_VERSION              :608.52                         
VDUT_VERSION             :                               
6.INPUT_MAC_ADDRESS __________________________________________________________
Test Time = 1.219 s
MAC_ADDRESS              :AC9A96F1BB21                   
SERIAL_NUMBER            :Hitron_test_2G                 
7.TX_XTAL_CALIBRATION 2442 MCS7 HE BW-20 TX1 _________________________________
[DEBUG_CAL]=> XTAL VALUE: 50[dec]	FREQ ERRO: 28.064[ppm] 
[DEBUG_CAL]=> XTAL VALUE: 200[dec]	FREQ ERRO: -44.758[ppm] 
[DEBUG_CAL]=> XTAL VALUE: 108[dec]	FREQ ERRO: -9.017[ppm] 
[DEBUG_CAL]=> XTAL VALUE: 90[dec]	FREQ ERRO: 0.725[ppm] 
Test Time = 0.483 s
FREQ_ERROR_AVG           :           0.73             ppm (-5.00, 2.00)
XTAL_DATA                :          90.00                 (, )
8.TX_CALIBRATION 2442 MCS7 HE BW-20 TX1 TX2 TX3 TX4 __________________________


[CAL]=>      Antenna 1:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:38 
[DEBUG_CAL]=> Region: 0  Gain: 6  Offset: 17 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.25  Volt:  8023  Evm: -42.80 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.59  Volt:  8454  Evm: -43.90 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  15.36  Volt:  8828  Evm: -42.25 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  16.49  Volt:  9241  Evm: -41.67 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  17.04  Volt:  9556  Evm: -40.78 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  18.05  Volt:  9927  Evm: -39.23 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.67  Volt: 10176  Evm: -38.18 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  19.99  Volt: 10586  Evm: -33.07 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  20.16  Volt: 10774  Evm: -32.36 
[DEBUG_CAL]=> INDEX_POWER: 44  PWR:  21.04  Volt: 11167  Evm: -28.09 
[DEBUG_CAL]=> INDEX_POWER: 46  PWR:  21.99  Volt: 11564  Evm: -27.07 
[DEBUG_CAL]=> INDEX_POWER: 48  PWR:  22.79  Volt: 11957  Evm: -23.77 
[DEBUG_CAL]=> INDEX_POWER: 50  PWR:  23.59  Volt: 12440  Evm: -21.42 
[DEBUG_CAL]=> INDEX_POWER: 52  PWR:  24.53  Volt: 12759  Evm: -19.64 


[CAL]=>      Antenna 2:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:37 
[DEBUG_CAL]=> Region: 0  Gain: 7  Offset: 18 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.35  Volt:  7278  Evm: -41.48 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.33  Volt:  7785  Evm: -40.70 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  15.31  Volt:  8330  Evm: -40.71 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  16.26  Volt:  8867  Evm: -40.57 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  16.91  Volt:  9207  Evm: -39.83 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  17.86  Volt:  9689  Evm: -39.03 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.54  Volt: 10021  Evm: -37.84 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  19.53  Volt: 10462  Evm: -34.80 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  19.95  Volt: 10717  Evm: -33.08 
[DEBUG_CAL]=> INDEX_POWER: 44  PWR:  20.87  Volt: 11172  Evm: -30.32 
[DEBUG_CAL]=> INDEX_POWER: 46  PWR:  21.78  Volt: 11817  Evm: -26.62 
[DEBUG_CAL]=> INDEX_POWER: 48  PWR:  22.63  Volt: 12167  Evm: -23.77 
[DEBUG_CAL]=> INDEX_POWER: 50  PWR:  23.47  Volt: 12752  Evm: -21.07 
[DEBUG_CAL]=> INDEX_POWER: 52  PWR:  24.15  Volt: 13118  Evm: -19.68 


[CAL]=>      Antenna 3:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:49 
[DEBUG_CAL]=> Region: 0  Gain: 7  Offset: 19 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.25  Volt:  8922  Evm: -39.23 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.52  Volt:  9394  Evm: -35.90 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  14.99  Volt:  9637  Evm: -35.20 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  15.80  Volt: 10201  Evm: -32.73 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  16.63  Volt: 10558  Evm: -29.19 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  17.62  Volt: 11008  Evm: -27.19 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.47  Volt: 11546  Evm: -24.70 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  19.45  Volt: 12147  Evm: -21.71 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  20.12  Volt: 12634  Evm: -20.26 


[CAL]=>      Antenna 4:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:33 
[DEBUG_CAL]=> Region: 0  Gain: 6  Offset: 17 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.01  Volt:  7704  Evm: -42.18 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.10  Volt:  8137  Evm: -41.56 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  15.10  Volt:  8471  Evm: -41.48 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  15.87  Volt:  8896  Evm: -41.78 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  16.84  Volt:  9259  Evm: -39.64 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  17.74  Volt:  9643  Evm: -39.21 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.91  Volt:  9957  Evm: -36.68 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  19.40  Volt: 10351  Evm: -36.46 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  19.99  Volt: 10592  Evm: -34.67 
[DEBUG_CAL]=> INDEX_POWER: 44  PWR:  20.86  Volt: 10959  Evm: -32.11 
[DEBUG_CAL]=> INDEX_POWER: 46  PWR:  21.44  Volt: 11177  Evm: -29.55 
[DEBUG_CAL]=> INDEX_POWER: 48  PWR:  22.29  Volt: 11555  Evm: -27.30 
[DEBUG_CAL]=> INDEX_POWER: 50  PWR:  23.33  Volt: 11935  Evm: -24.69 
[DEBUG_CAL]=> INDEX_POWER: 52  PWR:  24.08  Volt: 12433  Evm: -22.14 
[DEBUG_CAL]=> INDEX_POWER: 54  PWR:  24.84  Volt: 12707  Evm: -20.46 
Test Time = 6.770 s
TX1_MAX_POWER_INDEX      :          45                    (, )
TX2_MAX_POWER_INDEX      :          45                    (, )
TX3_MAX_POWER_INDEX      :          37                    (, )
TX4_MAX_POWER_INDEX      :          46                    (, )
TX1_MAX_POWER            :          22.49             dBm (, )
TX1_MAX_POWER_EVM        :         -25.00              dB (, )
TX1_ULTIMATE_EVM         :         -36.00              dB (, )
TX1_ULTIMATE_EVM_POWER   :          19.24             dBm (, )
TX2_MAX_POWER            :          22.26             dBm (, )
TX2_MAX_POWER_EVM        :         -25.00              dB (, )
TX2_ULTIMATE_EVM         :         -36.00              dB (, )
TX2_ULTIMATE_EVM_POWER   :          19.14             dBm (, )
TX3_MAX_POWER            :          18.36             dBm (, )
TX3_MAX_POWER_EVM        :         -25.00              dB (, )
TX3_ULTIMATE_EVM         :         -36.00              dB (, )
TX3_ULTIMATE_EVM_POWER   :          14.48             dBm (, )
TX4_MAX_POWER            :          23.21             dBm (, )
TX4_MAX_POWER_EVM        :         -25.00              dB (, )
TX4_ULTIMATE_EVM         :         -36.00              dB (, )
TX4_ULTIMATE_EVM_POWER   :          19.55             dBm (, )
9.TX_CALIBRATION 2442 MCS7 HE BW-40 TX1 TX2 TX3 TX4 __________________________


[CAL]=>      Antenna 1:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:38 
[DEBUG_CAL]=> Region: 0  Gain: 6  Offset: 17 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.46  Volt:  7049  Evm: -40.56 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.45  Volt:  7412  Evm: -40.07 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  15.69  Volt:  7781  Evm: -39.77 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  16.44  Volt:  8153  Evm: -37.86 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  17.21  Volt:  8443  Evm: -35.97 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  18.11  Volt:  8803  Evm: -36.32 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.68  Volt:  9088  Evm: -35.20 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  19.69  Volt:  9438  Evm: -32.20 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  20.17  Volt:  9620  Evm: -30.55 
[DEBUG_CAL]=> INDEX_POWER: 44  PWR:  21.08  Volt: 10000  Evm: -28.75 
[DEBUG_CAL]=> INDEX_POWER: 46  PWR:  21.93  Volt: 10388  Evm: -25.08 
[DEBUG_CAL]=> INDEX_POWER: 48  PWR:  22.74  Volt: 10848  Evm: -22.70 
[DEBUG_CAL]=> INDEX_POWER: 50  PWR:  23.52  Volt: 11105  Evm: -21.03 


[CAL]=>      Antenna 2:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:37 
[DEBUG_CAL]=> Region: 0  Gain: 7  Offset: 18 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.15  Volt:  6054  Evm: -40.27 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.24  Volt:  6527  Evm: -41.08 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  15.28  Volt:  7066  Evm: -40.25 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  16.24  Volt:  7497  Evm: -39.66 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  17.00  Volt:  7917  Evm: -38.28 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  18.08  Volt:  8302  Evm: -36.78 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.71  Volt:  8666  Evm: -36.33 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  19.65  Volt:  9156  Evm: -32.21 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  20.21  Volt:  9396  Evm: -30.59 
[DEBUG_CAL]=> INDEX_POWER: 44  PWR:  21.25  Volt:  9803  Evm: -26.63 
[DEBUG_CAL]=> INDEX_POWER: 46  PWR:  21.76  Volt: 10284  Evm: -25.43 
[DEBUG_CAL]=> INDEX_POWER: 48  PWR:  22.52  Volt: 10675  Evm: -22.71 
[DEBUG_CAL]=> INDEX_POWER: 50  PWR:  23.29  Volt: 11166  Evm: -20.91 
[DEBUG_CAL]=> INDEX_POWER: 52  PWR:  23.98  Volt: 11877  Evm: -19.72 


[CAL]=>      Antenna 3:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:49 
[DEBUG_CAL]=> Region: 0  Gain: 7  Offset: 19 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.37  Volt:  7578  Evm: -35.78 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.43  Volt:  8087  Evm: -34.16 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  14.80  Volt:  8320  Evm: -33.21 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  15.63  Volt:  8769  Evm: -31.08 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  16.60  Volt:  9296  Evm: -27.89 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  17.23  Volt:  9699  Evm: -26.85 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.12  Volt: 10099  Evm: -23.74 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  18.90  Volt: 10528  Evm: -21.57 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  19.80  Volt: 11014  Evm: -19.80 


[CAL]=>      Antenna 4:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:33 
[DEBUG_CAL]=> Region: 0  Gain: 6  Offset: 17 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  12.99  Volt:  6804  Evm: -39.78 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.15  Volt:  7197  Evm: -39.61 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  15.11  Volt:  7434  Evm: -39.85 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  15.81  Volt:  7892  Evm: -38.87 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  17.02  Volt:  8175  Evm: -38.38 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  17.67  Volt:  8570  Evm: -37.13 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.41  Volt:  8808  Evm: -35.68 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  19.31  Volt:  9162  Evm: -34.04 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  20.10  Volt:  9405  Evm: -33.97 
[DEBUG_CAL]=> INDEX_POWER: 44  PWR:  20.87  Volt:  9833  Evm: -29.77 
[DEBUG_CAL]=> INDEX_POWER: 46  PWR:  21.26  Volt: 10002  Evm: -28.85 
[DEBUG_CAL]=> INDEX_POWER: 48  PWR:  22.11  Volt: 10371  Evm: -26.40 
[DEBUG_CAL]=> INDEX_POWER: 50  PWR:  22.94  Volt: 10742  Evm: -24.81 
[DEBUG_CAL]=> INDEX_POWER: 52  PWR:  23.75  Volt: 11139  Evm: -21.76 
Test Time = 7.106 s
TX1_MAX_POWER_INDEX      :          44                    (, )
TX2_MAX_POWER_INDEX      :          44                    (, )
TX3_MAX_POWER_INDEX      :          36                    (, )
TX4_MAX_POWER_INDEX      :          46                    (, )
TX1_MAX_POWER            :          21.95             dBm (, )
TX1_MAX_POWER_EVM        :         -25.00              dB (, )
TX1_ULTIMATE_EVM         :         -36.00              dB (, )
TX1_ULTIMATE_EVM_POWER   :          18.27             dBm (, )
TX2_MAX_POWER            :          21.88             dBm (, )
TX2_MAX_POWER_EVM        :         -25.00              dB (, )
TX2_ULTIMATE_EVM         :         -36.00              dB (, )
TX2_ULTIMATE_EVM_POWER   :          18.79             dBm (, )
TX3_MAX_POWER            :          17.76             dBm (, )
TX3_MAX_POWER_EVM        :         -25.00              dB (, )
TX3_ULTIMATE_EVM         :         -35.78              dB (, )
TX3_ULTIMATE_EVM_POWER   :          13.37             dBm (, )
TX4_MAX_POWER            :          22.84             dBm (, )
TX4_MAX_POWER_EVM        :         -25.00              dB (, )
TX4_ULTIMATE_EVM         :         -36.00              dB (, )
TX4_ULTIMATE_EVM_POWER   :          18.25             dBm (, )
10.TX_CALIBRATION 2442 CCK-11 NON_HT BW-20 TX1 TX2 TX3 TX4 ___________________


[CAL]=>      Antenna 1:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:40 
[DEBUG_CAL]=> Region: 0  Gain: 6  Offset: 17 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.25  Volt:  7273  Evm: -31.58 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.27  Volt:  7657  Evm: -31.42 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  15.40  Volt:  8032  Evm: -31.71 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  15.92  Volt:  8293  Evm: -31.47 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  16.87  Volt:  8649  Evm: -31.65 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  17.45  Volt:  8903  Evm: -31.64 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.49  Volt:  9282  Evm: -31.74 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  18.89  Volt:  9492  Evm: -31.42 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  19.81  Volt:  9849  Evm: -31.51 
[DEBUG_CAL]=> INDEX_POWER: 44  PWR:  20.85  Volt: 10273  Evm: -31.81 
[DEBUG_CAL]=> INDEX_POWER: 46  PWR:  21.70  Volt: 10674  Evm: -31.95 
[DEBUG_CAL]=> INDEX_POWER: 48  PWR:  22.62  Volt: 10954  Evm: -31.69 
[DEBUG_CAL]=> INDEX_POWER: 50  PWR:  23.39  Volt: 11310  Evm: -31.79 
[DEBUG_CAL]=> INDEX_POWER: 52  PWR:  24.27  Volt: 11668  Evm: -31.23 


[CAL]=>      Antenna 2:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:39 
[DEBUG_CAL]=> Region: 0  Gain: 7  Offset: 18 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.07  Volt:  6344  Evm: -33.86 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  14.20  Volt:  6843  Evm: -33.71 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  15.07  Volt:  7335  Evm: -33.77 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  15.81  Volt:  7615  Evm: -33.92 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  16.69  Volt:  8072  Evm: -33.87 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  17.29  Volt:  8360  Evm: -33.73 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.22  Volt:  8846  Evm: -33.75 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  18.68  Volt:  9130  Evm: -33.71 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  19.62  Volt:  9509  Evm: -33.61 
[DEBUG_CAL]=> INDEX_POWER: 44  PWR:  20.63  Volt: 10054  Evm: -34.39 
[DEBUG_CAL]=> INDEX_POWER: 46  PWR:  21.62  Volt: 10474  Evm: -34.21 
[DEBUG_CAL]=> INDEX_POWER: 48  PWR:  22.59  Volt: 10920  Evm: -33.81 
[DEBUG_CAL]=> INDEX_POWER: 50  PWR:  23.25  Volt: 11359  Evm: -33.68 
[DEBUG_CAL]=> INDEX_POWER: 52  PWR:  24.06  Volt: 11874  Evm: -32.42 


[CAL]=>      Antenna 3:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:52 
[DEBUG_CAL]=> Region: 0  Gain: 7  Offset: 19 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.47  Volt:  8105  Evm: -35.80 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  13.99  Volt:  8297  Evm: -36.01 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  14.95  Volt:  8703  Evm: -35.64 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  16.04  Volt:  9145  Evm: -36.00 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  16.54  Volt:  9606  Evm: -35.88 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  17.34  Volt: 10015  Evm: -35.42 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.22  Volt: 10431  Evm: -34.76 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  19.08  Volt: 10962  Evm: -33.90 


[CAL]=>      Antenna 4:
 [DEBUG_CAL]=> GAIN TABLE CALIBRATION RESULT: INDEX:35, GAIN:35 
[DEBUG_CAL]=> Region: 0  Gain: 6  Offset: 17 
[DEBUG_CAL]=> INDEX_POWER: 26  PWR:  13.00  Volt:  7023  Evm: -31.16 
[DEBUG_CAL]=> INDEX_POWER: 28  PWR:  13.81  Volt:  7338  Evm: -30.89 
[DEBUG_CAL]=> INDEX_POWER: 30  PWR:  14.73  Volt:  7778  Evm: -31.05 
[DEBUG_CAL]=> INDEX_POWER: 32  PWR:  15.76  Volt:  8028  Evm: -30.83 
[DEBUG_CAL]=> INDEX_POWER: 34  PWR:  16.60  Volt:  8428  Evm: -31.18 
[DEBUG_CAL]=> INDEX_POWER: 36  PWR:  17.23  Volt:  8697  Evm: -30.70 
[DEBUG_CAL]=> INDEX_POWER: 38  PWR:  18.33  Volt:  9030  Evm: -31.09 
[DEBUG_CAL]=> INDEX_POWER: 40  PWR:  18.95  Volt:  9276  Evm: -30.71 
[DEBUG_CAL]=> INDEX_POWER: 42  PWR:  19.74  Volt:  9689  Evm: -31.12 
[DEBUG_CAL]=> INDEX_POWER: 44  PWR:  20.19  Volt:  9860  Evm: -30.72 
[DEBUG_CAL]=> INDEX_POWER: 46  PWR:  21.22  Volt: 10194  Evm: -30.97 
[DEBUG_CAL]=> INDEX_POWER: 48  PWR:  22.07  Volt: 10642  Evm: -31.52 
[DEBUG_CAL]=> INDEX_POWER: 50  PWR:  23.14  Volt: 10985  Evm: -31.56 
[DEBUG_CAL]=> INDEX_POWER: 52  PWR:  23.98  Volt: 11316  Evm: -31.73 
[DEBUG_CAL]=> INDEX_POWER: 54  PWR:  24.61  Volt: 11654  Evm: -31.12 
Test Time = 13.189 s
TX1_MAX_POWER_INDEX      :          49                    (, )
TX2_MAX_POWER_INDEX      :          48                    (, )
TX3_MAX_POWER_INDEX      :          38                    (, )
TX4_MAX_POWER_INDEX      :          34                    (, )
TX1_MAX_POWER            :          24.27             dBm (, )
TX1_MAX_POWER_EVM        :         -31.23              dB (, -25.00)
TX1_ULTIMATE_EVM         :         -31.95              dB (, )
TX1_ULTIMATE_EVM_POWER   :          21.70             dBm (, )
TX2_MAX_POWER            :          24.06             dBm (, )
TX2_MAX_POWER_EVM        :         -32.42              dB (, -25.00)
TX2_ULTIMATE_EVM         :         -34.39              dB (, )
TX2_ULTIMATE_EVM_POWER   :          20.63             dBm (, )
TX3_MAX_POWER            :          19.08             dBm (, )
TX3_MAX_POWER_EVM        :         -33.90              dB (, -25.00)
TX3_ULTIMATE_EVM         :         -36.00              dB (, )
TX3_ULTIMATE_EVM_POWER   :          16.05             dBm (, )
TX4_MAX_POWER            :          17.23             dBm (, )
TX4_MAX_POWER_EVM        :         -30.70              dB (, -25.00)
TX4_ULTIMATE_EVM         :         -31.73              dB (, )
TX4_ULTIMATE_EVM_POWER   :          23.98             dBm (, )
11.RX_CALIBRATION BW-40 RX1 RX2 RX3 RX4 ______________________________________


[DEBUG_CAL]=>    RX Mid Gain Calibration 
[DEBUG_CAL]=> ANT: 1 CALC_GAIN_CTRL:12 Min: 0.00  Max 15.00 
[DEBUG_CAL]=> ANT: 1 TARGET_GAIN:38.00 Min: 37.00 Max 39.00 Measure-> :37.63 
[DEBUG_CAL]=> ANT: 2 CALC_GAIN_CTRL:12 Min: 0.00  Max 15.00 
[DEBUG_CAL]=> ANT: 2 TARGET_GAIN:38.00 Min: 37.00 Max 39.00 Measure-> :38.09 
[DEBUG_CAL]=> ANT: 3 CALC_GAIN_CTRL:15 Min: 0.00  Max 15.00 
[DEBUG_CAL]=> ANT: 3 TARGET_GAIN:38.00 Min: 37.00 Max 39.00 Measure-> :35.47 
[DEBUG_CAL]=> ANT: 4 CALC_GAIN_CTRL:12 Min: 0.00  Max 15.00 
[DEBUG_CAL]=> ANT: 4 TARGET_GAIN:38.00 Min: 37.00 Max 39.00 Measure-> :37.81 


[DEBUG_CAL]=>    RX Gain Accuracy Calibration 
[DEBUG_CAL]=> ANT: 1 LNA_GAIN:0  Measure Gain -> :40.94 
[DEBUG_CAL]=> ANT: 1 LNA_GAIN:1  Measure Gain -> :37.59 
[DEBUG_CAL]=> ANT: 1 LNA_GAIN:2  Measure Gain -> :30.94 
[DEBUG_CAL]=> ANT: 1 LNA_GAIN:3  Measure Gain -> :24.63 
[DEBUG_CAL]=> ANT: 1 LNA_GAIN:4  Measure Gain -> :14.91 
[DEBUG_CAL]=> ANT: 1 LNA_GAIN:5  Measure Gain -> :8.22 
[DEBUG_CAL]=> ANT: 2 LNA_GAIN:0  Measure Gain -> :41.50 
[DEBUG_CAL]=> ANT: 2 LNA_GAIN:1  Measure Gain -> :38.16 
[DEBUG_CAL]=> ANT: 2 LNA_GAIN:2  Measure Gain -> :31.41 
[DEBUG_CAL]=> ANT: 2 LNA_GAIN:3  Measure Gain -> :25.28 
[DEBUG_CAL]=> ANT: 2 LNA_GAIN:4  Measure Gain -> :15.56 
[DEBUG_CAL]=> ANT: 2 LNA_GAIN:5  Measure Gain -> :8.97 
[DEBUG_CAL]=> ANT: 3 LNA_GAIN:0  Measure Gain -> :35.50 
[DEBUG_CAL]=> ANT: 3 LNA_GAIN:1  Measure Gain -> :35.34 
[DEBUG_CAL]=> ANT: 3 LNA_GAIN:2  Measure Gain -> :28.41 
[DEBUG_CAL]=> ANT: 3 LNA_GAIN:3  Measure Gain -> :22.00 
[DEBUG_CAL]=> ANT: 3 LNA_GAIN:4  Measure Gain -> :12.84 
[DEBUG_CAL]=> ANT: 3 LNA_GAIN:5  Measure Gain -> :5.97 
[DEBUG_CAL]=> ANT: 4 LNA_GAIN:0  Measure Gain -> :41.22 
[DEBUG_CAL]=> ANT: 4 LNA_GAIN:1  Measure Gain -> :37.88 
[DEBUG_CAL]=> ANT: 4 LNA_GAIN:2  Measure Gain -> :31.22 
[DEBUG_CAL]=> ANT: 4 LNA_GAIN:3  Measure Gain -> :25.00 
[DEBUG_CAL]=> ANT: 4 LNA_GAIN:4  Measure Gain -> :15.66 
[DEBUG_CAL]=> ANT: 4 LNA_GAIN:5  Measure Gain -> :9.06 


[DEBUG_CAL]=>    S2D Calibration 
[DEBUG_CAL]=> ANT: 1 REGION:0  Measure S2D Gain -> :18 
[DEBUG_CAL]=> ANT: 1 REGION:0  Measure S2D Offset -> :18 
[DEBUG_CAL]=> ANT: 1 REGION:1  Measure S2D Gain -> :12 
[DEBUG_CAL]=> ANT: 1 REGION:1  Measure S2D Offset -> :21 
[DEBUG_CAL]=> ANT: 2 REGION:0  Measure S2D Gain -> :18 
[DEBUG_CAL]=> ANT: 2 REGION:0  Measure S2D Offset -> :14 
[DEBUG_CAL]=> ANT: 2 REGION:1  Measure S2D Gain -> :12 
[DEBUG_CAL]=> ANT: 2 REGION:1  Measure S2D Offset -> :16 
[DEBUG_CAL]=> ANT: 3 REGION:0  Measure S2D Gain -> :18 
[DEBUG_CAL]=> ANT: 3 REGION:0  Measure S2D Offset -> :15 
[DEBUG_CAL]=> ANT: 3 REGION:1  Measure S2D Gain -> :12 
[DEBUG_CAL]=> ANT: 3 REGION:1  Measure S2D Offset -> :17 
[DEBUG_CAL]=> ANT: 4 REGION:0  Measure S2D Gain -> :18 
[DEBUG_CAL]=> ANT: 4 REGION:0  Measure S2D Offset -> :13 
[DEBUG_CAL]=> ANT: 4 REGION:1  Measure S2D Gain -> :12 
[DEBUG_CAL]=> ANT: 4 REGION:1  Measure S2D Offset -> :15 


[DEBUG_CAL]=>    AB Calibration 
[DEBUG_CAL]=> ANT: 1 REGION:0  Measure A -> :1.83 
[DEBUG_CAL]=> ANT: 1 REGION:0  Measure B -> :-29.94 
[DEBUG_CAL]=> ANT: 1 REGION:1  Measure A -> :1.99 
[DEBUG_CAL]=> ANT: 1 REGION:1  Measure B -> :-26.60 
[DEBUG_CAL]=> ANT: 2 REGION:0  Measure A -> :1.53 
[DEBUG_CAL]=> ANT: 2 REGION:0  Measure B -> :-30.65 
[DEBUG_CAL]=> ANT: 2 REGION:1  Measure A -> :1.96 
[DEBUG_CAL]=> ANT: 2 REGION:1  Measure B -> :-26.14 
[DEBUG_CAL]=> ANT: 3 REGION:0  Measure A -> :1.25 
[DEBUG_CAL]=> ANT: 3 REGION:0  Measure B -> :-34.29 
[DEBUG_CAL]=> ANT: 3 REGION:1  Measure A -> :2.02 
[DEBUG_CAL]=> ANT: 3 REGION:1  Measure B -> :-26.21 
[DEBUG_CAL]=> ANT: 4 REGION:0  Measure A -> :1.63 
[DEBUG_CAL]=> ANT: 4 REGION:0  Measure B -> :-30.82 
[DEBUG_CAL]=> ANT: 4 REGION:1  Measure A -> :2.01 
[DEBUG_CAL]=> ANT: 4 REGION:1  Measure B -> :-26.69 
Test Time = 2.855 s
ERROR_MESSAGE            :                               
12.BURN_CALIBRATION __________________________________________________________
[DEBUG_CAL]=> Channel: 7 BW: 0 
[DEBUG_CAL]=> ANT: 0 REG: 0 ->A: 617 B: -11 Gain: 6 Offset: 17 
[DEBUG_CAL]=> ANT: 1 REG: 0 ->A: 490 B: 0 Gain: 7 Offset: 18 
[DEBUG_CAL]=> ANT: 2 REG: 0 ->A: 480 B: -6 Gain: 7 Offset: 19 
[DEBUG_CAL]=> ANT: 3 REG: 0 ->A: 618 B: -10 Gain: 6 Offset: 17 
[DEBUG_CAL]=> Channel: 5 BW: 1 
[DEBUG_CAL]=> ANT: 0 REG: 0 ->A: 642 B: -7 Gain: 6 Offset: 17 
[DEBUG_CAL]=> ANT: 1 REG: 0 ->A: 507 B: 4 Gain: 7 Offset: 18 
[DEBUG_CAL]=> ANT: 2 REG: 0 ->A: 486 B: -1 Gain: 7 Offset: 19 
[DEBUG_CAL]=> ANT: 3 REG: 0 ->A: 648 B: -7 Gain: 6 Offset: 17 
[DEBUG_CAL]=> Channel: 7 BW: 3 
[DEBUG_CAL]=> ANT: 0 REG: 0 ->A: 652 B: -9 Gain: 6 Offset: 17 
[DEBUG_CAL]=> ANT: 1 REG: 0 ->A: 528 B: 1 Gain: 7 Offset: 18 
[DEBUG_CAL]=> ANT: 2 REG: 0 ->A: 506 B: -4 Gain: 7 Offset: 19 
[DEBUG_CAL]=> ANT: 3 REG: 0 ->A: 664 B: -9 Gain: 6 Offset: 17 
Test Time = 0.109 s
TX_ERROR_ANT1_REG1       :           0.58                 (, )
TX_ERROR_ANT2_REG1       :           0.99                 (, )
TX_ERROR_ANT3_REG1       :           0.46                 (, )
TX_ERROR_ANT4_REG1       :           0.47                 (, )
13.FLUSH_MEMORY ______________________________________________________________
Test Time = 0.253 s
14.TX_VERIFY_EVM 2442 MCS11 HE BW-20 TX1 TX2 TX3 TX4 _________________________
Test Time = 1.367 s
SPATIAL_STREAM           :           1                    (, )
DATA_RATE                :         143.40            Mbps (, )
EVM_AVG_1                :         -35.24              dB (, -20.00)
EVM_AVG_2                :         -33.66              dB (, -20.00)
EVM_AVG_3                :         -29.39              dB (, -20.00)
EVM_AVG_4                :         -33.75              dB (, -20.00)
FREQ_ERROR_AVG           :           0.62             ppm (-20.00, 20.00)
LO_LEAKAGE_DB_1          :         -36.15             dBc (, -20.00)
LO_LEAKAGE_DB_2          :         -29.01             dBc (, -20.00)
LO_LEAKAGE_DB_3          :         -38.45             dBc (, -20.00)
LO_LEAKAGE_DB_4          :         -37.94             dBc (, -20.00)
POWER_AVG_1              :          20.00             dBm (18.50, 21.50)
POWER_AVG_2              :          19.90             dBm (18.50, 21.50)
POWER_AVG_3              :          19.98             dBm (18.50, 21.50)
POWER_AVG_4              :          20.09             dBm (18.50, 21.50)
TX_POWER_DBM             :          20.00             dBm (, )
15.TX_VERIFY_EVM 2442 MCS11 HE BW-40 TX1 TX2 TX3 TX4 _________________________
Test Time = 1.377 s
SPATIAL_STREAM           :           1                    (, )
DATA_RATE                :         286.80            Mbps (, )
EVM_AVG_1                :         -33.62              dB (, -20.00)
EVM_AVG_2                :         -33.87              dB (, -20.00)
EVM_AVG_3                :         -29.06              dB (, -20.00)
EVM_AVG_4                :         -35.30              dB (, -20.00)
FREQ_ERROR_AVG           :           0.69             ppm (-20.00, 20.00)
LO_LEAKAGE_DB_1          :         -37.95             dBc (, -20.00)
LO_LEAKAGE_DB_2          :         -37.49             dBc (, -20.00)
LO_LEAKAGE_DB_3          :         -49.57             dBc (, -20.00)
LO_LEAKAGE_DB_4          :         -37.51             dBc (, -20.00)
POWER_AVG_1              :          19.87             dBm (18.50, 21.50)
POWER_AVG_2              :          19.85             dBm (18.50, 21.50)
POWER_AVG_3              :          19.56             dBm (18.50, 21.50)
POWER_AVG_4              :          19.99             dBm (18.50, 21.50)
TX_POWER_DBM             :          20.00             dBm (, )
16.TX_VERIFY_EVM 2442 CCK-11 NON_HT BW-20 TX1 TX2 TX3 TX4 ____________________
Test Time = 3.356 s
SPATIAL_STREAM           :           1                    (, )
DATA_RATE                :          11.00            Mbps (, )
EVM_AVG_1                :         -38.61              dB (, -20.00)
EVM_AVG_2                :         -33.47              dB (, -20.00)
EVM_AVG_3                :         -36.38              dB (, -20.00)
EVM_AVG_4                :         -34.05              dB (, -20.00)
FREQ_ERROR_AVG           :           0.60             ppm (-20.00, 20.00)
LO_LEAKAGE_DB            :         -31.59             dBc (, -20.00)
POWER_AVG_1              :          19.96             dBm (18.50, 21.50)
POWER_AVG_2              :          19.92             dBm (18.50, 21.50)
POWER_AVG_3              :          19.76             dBm (18.50, 21.50)
POWER_AVG_4              :          19.95             dBm (18.50, 21.50)
TX_POWER_DBM             :          20.00             dBm (, )
17.RX_VERIFY_PER 2442 MCS7 HE BW-20 RX1 RX2 RX3 RX4 __________________________
Test Time = 0.669 s
GOOD_PACKETS             :         335                    (, )
TOTAL_PACKETS            :         500                    (, )
PER                      :          33.00               % (, )
RSSI_1                   :         -61.00                 (, )
RSSI_2                   :         -61.00                 (, )
RSSI_3                   :         -61.00                 (, )
RSSI_4                   :         -60.00                 (, )
RX_POWER_LEVEL           :         -60.00             dBm (, )
WAVEFORM_NAME            :./mod/WiFi_11AX_HE20_S1_MCS7.iqvsg      
18.RX_VERIFY_PER 2442 MCS7 HE BW-40 RX1 RX2 RX3 RX4 __________________________
Test Time = 0.512 s
GOOD_PACKETS             :         257                    (, )
TOTAL_PACKETS            :         500                    (, )
PER                      :          48.60               % (, )
RSSI_1                   :         -60.00                 (, )
RSSI_2                   :         -57.00                 (, )
RSSI_3                   :         -57.00                 (, )
RSSI_4                   :         -59.00                 (, )
RX_POWER_LEVEL           :         -60.00             dBm (, )
WAVEFORM_NAME            :./mod/WiFi_11AX_HE40_S1_MCS7.iqvsg      
19.REMOVE_DUT ________________________________________________________________
Test Time = 0.001 s
20.DISCONNECT_IQ_TESTER ______________________________________________________
Test Time = 0.000 s
ERROR_MESSAGE            :[Info] Function completed.
      
--------------------------------------------------------------------
                         *************
                         *  P A S S  *
                         *************
1.CONNECT_IQ_TESTER                             :0.340 s                     
2.DEFAULT_GLOBAL_SETTINGS                       :0.000 s                     
3.GLOBAL_SETTINGS                               :0.000 s                     
4.LOAD_PATH_LOSS_TABLE                          :0.000 s                     
5.INSERT_DUT                                    :0.616 s                     
6.INPUT_MAC_ADDRESS                             :1.219 s                     
7.TX_XTAL_CALIBRATION 2442 MCS7 HE BW-20 TX1    :0.483 s                     
8.TX_CALIBRATION 2442 MCS7 HE BW-20 TX1 TX2 TX3 TX4   :6.770 s                     
9.TX_CALIBRATION 2442 MCS7 HE BW-40 TX1 TX2 TX3 TX4   :7.106 s                     
10.TX_CALIBRATION 2442 CCK-11 NON_HT BW-20 TX1 TX2 TX3 TX4   :13.189 s                    
11.RX_CALIBRATION BW-40 RX1 RX2 RX3 RX4         :2.855 s                     
12.BURN_CALIBRATION                             :0.109 s                     
13.FLUSH_MEMORY                                 :0.253 s                     
14.TX_VERIFY_EVM 2442 MCS11 HE BW-20 TX1 TX2 TX3 TX4   :1.367 s                     
15.TX_VERIFY_EVM 2442 MCS11 HE BW-40 TX1 TX2 TX3 TX4   :1.377 s                     
16.TX_VERIFY_EVM 2442 CCK-11 NON_HT BW-20 TX1 TX2 TX3 TX4   :3.356 s                     
17.RX_VERIFY_PER 2442 MCS7 HE BW-20 RX1 RX2 RX3 RX4   :0.669 s                     
18.RX_VERIFY_PER 2442 MCS7 HE BW-40 RX1 RX2 RX3 RX4   :0.512 s                     
19.REMOVE_DUT                                   :0.001 s                     
20.DISCONNECT_IQ_TESTER                         :0.000 s                     
Total Run(s)                :1
Passed Run(s)               :1
Failed Run(s) on Limits     :0
Failed Run(s) on Errors     :0
In This Run:
    Fail(s) on Limits       :0
    Fail(s) on Errors       :0
    Flow Run Time           :42.006 s

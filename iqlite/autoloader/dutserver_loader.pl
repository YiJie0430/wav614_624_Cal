#!/usr/bin/perl -w


use strict;
use FindBin; # to findout the path of the script
use lib "$FindBin::Bin/libraries";
use Cwd;
use Getopt::Long;
use Loader;
use TelnetClient;
use File::Copy;
my $myName = "dutserver_loader";   # Or use $0 for the full app name with path

$| = 1; # avoid buffering in printing

################################
##   global variables
################################


################################
##    commandline options
################################

my $intermediateDir;
my $target_ip;
my $tftp_port;
my $dutOnly;
my $localIP;
my $reset;
my $tftp;
my $calibration_file;

sub Usage1
{
    print "Usage:\n$myName\n";
    print " [-ip <target IP> default: ask]\n";
    print " [-dir <intermediate (local) directory> default: <temp dir>]\n";
    print " [-dut] - load device under test files from current directory unless -dir is set\n";
    exit;
}

sub GetBinsSubdir
{
    my $hardware_id=Loader::GetHWId();
    if($hardware_id eq "0700") #Gen3 PCI
    {
      return "Gen3";
    }
    elsif($hardware_id eq "0710") #Gen3 PCIE
    {
      return "Gen3";
    }
    elsif( ($hardware_id eq "0680") or ($hardware_id eq "0681") ) #Gen2 PCI
    {
      return "Gen2";
    }
    else
    {
      print "unknown hardware ID \"$hardware_id\" or hardware not found";
      sleep(5);
      die "unknown hardware ID \"$hardware_id\"";
    }
}

GetOptions (
            'directory:s' => \$intermediateDir,  #not required(:) string (s)
            'ip:s'        => \$target_ip,
            'dut'         => \$dutOnly,
            'reset'        => \$reset,
            'tftp'        => \$tftp,
            'radio:s'        => \$calibration_file,
            'help|?'         => \&Usage1,
           );

if (!$dutOnly && !$reset && !$tftp)
{
	print "This autoloader is used only for DUT. Use -dut -reset or -tftp option\n";
	sleep(2);
	die "This autoloader is used only for DUT. Use -dut -reset or -tftp option\n";
}

if (!$intermediateDir)
{
  $intermediateDir = cwd;
}
Colors::PrintRichtext("<green>--- <b>Loader script for DUT Server</> ---</>\n\n");

if (!$target_ip)
{
  die "IP address must be specified. Use -ip option\n";
}
  #TODO - autoselect a free port
  $tftp_port = 2559;
  

 my $ret = Loader::ConnectToTarget($target_ip,$tftp_port,$localIP);
 if ($ret!=1)
 {
	# we couldn't connect.
	Colors::PrintRichtext("\n<red>$ret</>n");
	print "\nPress Enter to Continue\n\n";
    my $command = <STDIN>;
	 die $ret;
 }
 # start tftp server thread.
 
 # wait a second for tftp to start ...
 sleep(1);

if ($reset)
{
	# do reset to the target
	my $ret = Loader::SendReboot();
	if ($dutOnly)
	{	
		#in case we also want dut, reconnect to target 
		$ret = Loader::ConnectToTarget($target_ip,$tftp_port,$localIP);
		if ($ret!=1)
		{
			# we couldn't connect.
			Colors::PrintRichtext("\n<red>$ret</>n");
			print "\nPress Enter to Continue\n\n";
			my $command = <STDIN>;
			die $ret;
		}
	}
}

if ($tftp)
{
	print("autoloader detect tftp with radio=".$calibration_file." IP= ".$target_ip."\n");
	Loader::StartTftpCalFile($calibration_file, $target_ip);
	#die "Test End";
}

if ($dutOnly) 
{
	Loader::StartDUTServer();
}
 
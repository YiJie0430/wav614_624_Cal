use strict;
use Win32::Console;

package Colors;

my $g_console = new Win32::Console(main::STD_OUTPUT_HANDLE);
my @g_attrStack;

sub AttrPush
{
  my $new_attr = shift;
  push(@g_attrStack, $g_console->Attr());
  $g_console->Attr($new_attr);
}
sub AttrPop
{
  $g_console->Attr(pop(@g_attrStack));
}

sub PrintColor
{
  my $color = shift;
  my $str   = shift;
  AttrPush($color);
  PrintRichtext($str);
  AttrPop();
}



sub PrintSystem
{
    my $what = shift;
    PrintColor($main::FG_GRAY, $what);
}

sub PrintPrompt
{
    my $what = shift;
    PrintColor($main::FG_LIGHTCYAN, $what);
}

sub Message
{
    my $what = shift;
    PrintColor($main::FG_GRAY, $what);
}
sub Error
{
    my $what = shift;
    PrintColor($main::FG_LIGHTRED, $what);
}

sub PrintRichtext
{
    my $what = shift;
    while($what)
    {
        if ($what =~ m/<([\/a-z]+?)>/i )    #catch tags of the form <abc>
        {
            my $tag  = $&;
            my $rest = $';
            my $pre  = $`;
            my $tag_text = $1;
            if ($pre =~ /(.*)\\$/){         #was the tag preceded by a backslash (esc sequence), not a real tag!
                print $1.$tag;            #without the backslah!
                $what = $rest;    #without the backslah!
                next;              
            }

            $what = $rest;

            print $pre;
            #print "|$tag_text|";
            OperateTag($tag_text);
            next;
        }
        print $what;
        last;
    }
}


sub OperateTag
{
    my $tag_text = shift;
    $tag_text = lc($tag_text);
    my $color;
    if ($tag_text eq "b" || $tag_text eq "bold")           #BOLD
    {
#        AttrPush($main::FG_LIGHTMAGENTA);
        AttrPush(Bold($g_console->Attr()));
    }elsif ($tag_text eq "inverse")     #Invers video
    {
        AttrPush(InverseVideo($g_console->Attr()));
    }elsif ($color = eval("\$main::FG_".uc($tag_text)))
    {
        AttrPush($color);
    }
    
    elsif ($tag_text eq "/")
    {
        AttrPop();
    }else{
        die "\nunknown TAG in richtext : $tag_text\n";
    }
}

sub Bold
{
    my $original = shift;
    if ($original)
    {
        return ($original | 0x08);
    }
    return 0x08;

}

sub InverseVideo
{
    my $original = shift;
    #swap nibbles (FG and BG)
    return (($original & 0x0f)<<4) | (($original & 0xf0)>>4);
}


1;

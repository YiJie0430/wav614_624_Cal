use strict;
use Colors;

package Loader;
use File::Copy;
use File::Find;
use TelnetClient;
use Socket;
use Sys::Hostname;
use FindBin; # to findout the path of the script
use lib "$FindBin::Bin";
use Cwd;
use Net::Ping;

my $telnet;
my $MyIP;
my $TargetIP;
my $MyPort;
my @IP;
my @Subnet;
BEGIN{
    $telnet = new TelnetClient;
    # find out our IP list
    open (INFO, "ipconfig |");
    my @lines = <INFO>;
    close( INFO );
    my $i = 0;
    my $j = 0;

    foreach my $line (@lines)
    {
      if ($line =~ /IP Address[^\d]*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/)
      {
        $IP[$i] = $1;
        $i = $i + 1;
      }
      if ($line =~ /Subnet Mask[^\d]*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/)
      {
        $Subnet[$j] = $1;
        $j = $j + 1;
      }

   }
   #my $host = hostname();
   #$MyIP = inet_ntoa(scalar gethostbyname($host || 'localhost'));

   $MyPort = 69;
}

###########################
##
##  LoadVersionsToTarget
##
##  loads all components (CPU FW, Driver, etc) into target.
##  Implemented for dongle (using TFTP)
##
###########################
sub ConnectToTarget
{
    $TargetIP = shift;
    $MyPort = shift;
    my $TempIP = shift;
    if ($TempIP)
    {
        $MyIP  = $TempIP
        
    }
    else
    {
        my $not_found = 1;
        # after connection, we can find out our IP that matches the targets IP
        my $ipMask = $TargetIP;
        for (my $i = 0;$i < @IP && $not_found; $i++)
        {
          if (inet_aton ($TargetIP)) # only if IP is valid
          {
            my $int_sn = inet_aton ($Subnet[$i]);
            my $int_ip = inet_ntoa(inet_aton ($TargetIP) & $int_sn);
            my $int_nw = inet_ntoa(inet_aton ($IP[$i]) & $int_sn);
            if ($int_ip  eq $int_nw )
            {
              $MyIP = $IP[$i];
              $not_found = 0;
            }
          }
        }
        use IO::Socket::INET;

        my $sock = IO::Socket::INET->new(PeerAddr=>$TargetIP,PeerPort=>80,Proto=>"tcp");
        if (defined($sock))
        {
            $MyIP = $sock->sockhost;
        }
        else
        {
           my $host = hostname();
           $MyIP = inet_ntoa(scalar gethostbyname($host || 'localhost'));
        }
    }
    print("my local IP address for target ".$TargetIP." is ".$MyIP."\n");

    my $ret = $telnet->Connect($TargetIP);
	if ($ret != 1) # connection failed
	{
		print("got ret= $ret\n");
		return $ret;
	}
    my $post_login = GetHost()->{"post_login"};
    if ($post_login)
    {
	require $post_login;
	DoPostLoginCommands($telnet);
    }
	return 1;
}

sub KillDriver
{
        my ($binariesLocation,$target_basedir) = @_;
	my $prePostModule = GetHost()->{"pre_post_rmmod_module"};
	if ($prePostModule) 
	{
		require $prePostModule;
		DoPreRmmodCommands($telnet, $binariesLocation); 
	}
        print("Stopping helper apps... \n");
#       	sleep(2);
#	$telnet->command("cd ".$target_basedir);
#	$telnet->command("brctl delif br0 wlan0 ");
#	$telnet->command("brctl delif br0 wlan1 ");

        # We try to kill two times to also catch scripts started after first kill
        my @ret = $telnet->commandarray("ps | grep wlan | awk '{print \$1}' ");
        print("wlan Processes: ".@ret."\n");
        if (@ret == 1) {
           @ret = $telnet->commandarray("ps | grep rc.common | awk '{print \$1}' ");
           if (@ret != 1) {
              print("rc.common detected. Waiting for WLAN init to start\n");
              my $count=0;
              WLANINIT: for  $count (1,2,3,4,5,6,7,8,9,10) {
                 sleep(1);
                 print($count."\n");
                 @ret = $telnet->commandarray("ps | grep wlan | awk '{print \$1}' ");
                 if (@ret != 1) {
                    last WLANINIT;
                 }
              }
              @ret = $telnet->commandarray("ps | grep wlan | awk '{print \$1}' ");
              print("2nd try for wlan Processes: ".@ret."\n");
              my $pid="";
              my @processlist = @ret;
              foreach $pid (@processlist) {
                 $telnet->command("kill ".$pid);
                 print("Process: ".$pid." killed\n");
              }
              @ret = $telnet->commandarray("ps | grep mtlk | awk '{print \$1}' ");
              print("mtlk Processes: ".@ret."\n");
              $pid="";
              @processlist = @ret;
              foreach $pid (@processlist) {
                 $telnet->command("kill ".$pid);
                 print("Process: ".$pid." killed\n");
              }
           }
        }
        else
        {
           my $pid="";
           my @processlist = @ret;
           foreach $pid (@processlist) {
              $telnet->command("kill ".$pid);
              print("Process: ".$pid." killed\n");
           }
           my @ret = $telnet->commandarray("ps | grep mtlk | awk '{print \$1}' ");
           print("mtlk Processes: ".@ret."\n");
           $pid="";
           @processlist = @ret;
           foreach $pid (@processlist) {
              $telnet->command("kill ".$pid);
              print("Process: ".$pid." killed\n");
           }
        }

#        sleep(60);
#	$telnet->command("killall mtlk_init.sh");
#	$telnet->command("killall rc.bringup_wlan");
	$telnet->command("killall wsccmd");
	$telnet->command("killall drvhlpr");
	$telnet->command("killall drvhlpr_wlan0");
	$telnet->command("killall drvhlpr_wlan1");
	$telnet->command("killall wpa_supplicant");
	$telnet->command("killall hostapd");
	$telnet->command("killall hostapd_wlan0");
	$telnet->command("killall hostapd_wlan1");
	print("Removing driver... ");
	$telnet->print("rmmod mtlk\n");
	print("Executing 'arp -d'...");
	system("arp -d");
	sleep(2);
	
	eval {
		$telnet->get();
	};
	
	if ($prePostModule)
	{ 
		DoPostRmmodCommands($telnet, $binariesLocation); 
	}
	print("done\n");

#	$telnet->print_history();
#	sleep(30);

};

sub CreateDUTdirs
{
        my ($target_basedir) = @_;
	$telnet->command("mkdir -p ".$target_basedir."/dut");
	if (defined(Loader::GetHost()->{"images_dir"}))
	{
   	   $telnet->command("mkdir -p ".Loader::GetHost()->{"images_dir"});
	   $telnet->command("rm -f ".Loader::GetHost()->{"images_dir"}."/*"); # Remove leftovers from previous autoloader, we might not have enough space to do it again...
	}
	if (defined(Loader::GetHost()->{"progmodels_dir"}))
	{
	   $telnet->command("mkdir -p ".Loader::GetHost()->{"progmodels_dir"});
  	   $telnet->command("rm -f ".Loader::GetHost()->{"progmodels_dir"}."/*"); # Remove leftovers from previous autoloader, we might not have enough space to do it again...
        }
}

sub LoadBinFile
{
  my ($localDir,$fileName,$remoteDir,$mode) = @_;
  $telnet->command("cd ".$remoteDir);
  $telnet->command("rm -f ".$fileName);
  my $command = "tftp -g -r $localDir/$fileName -l $fileName $MyIP $MyPort";
  my $ret = $telnet->command($command);
  $ret =~ s/tftp\: timeout//g;

  if ($ret =~ m/tftp/)
  {
    Colors::Error("Failed to load $fileName: '$ret'\n");
	sleep(5);
    exit(1);
  }
  if ($mode)
  {
	$telnet->command("chmod ".$mode." ".$fileName);
  }
}

sub StartDUT
{
        my ($target_basedir, $images_dir) = @_;
	Colors::PrintRichtext("<white>Starting DUT driver, please wait...</>\n");
	#$telnet->command("rm -f ".$target_basedir."/contr_lm.bin");
	#$telnet->command("ln -s ".$images_dir."/d/contr_lm.bin ".$target_basedir."/contr_lm.bin");
	#$telnet->command("ln -s ".$images_dir."/d/contr_um.bin ".$target_basedir."/contr_um.bin");
	$telnet->command("cd ".$target_basedir);
	if(GetHost()->{"uncompressed_download"} eq "true")
        {
   	    $telnet->command('echo "./dutserver -u '.$telnet->GetHost()->{"driver_name"}.' &" >run_dut.sh');
        }
        else
        {
   	    $telnet->command('echo "./dutserver '.$telnet->GetHost()->{"driver_name"}.' &" >run_dut.sh');
        }
	$telnet->command("chmod +x run_dut.sh");
	$telnet->print("/bin/sh run_dut.sh");
        sleep(1);
}

sub StartDUTServer
{
	Colors::PrintRichtext("<white>Starting DUT driver, please wait...</>\n");
	 my $exe = GetHost()->{"dut_exe"};
	 if ($exe eq '') {
		Colors::PrintRichtext("<red>could not find dutserver ! please check linux_hosts.txt file </>\n");
		sleep (2);
		exit(1);
	 }
	 	
	#Debug mode: if files are in "/tmp", probably debug mode. Save in /tmp/dut_aps_and_scripts.sh to be use by wlan scripts:
	my $dut_path = GetHost()->{"scripts_path"};
	my $is_tmp_folder = substr($dut_path, 0, 5);
	if ($is_tmp_folder eq "/tmp/")
	{
		my $drvhlpr_path = GetHost()->{"drvhlpr_app"};
		$telnet->command('cd /tmp');
		#$telnet->command('echo "'.$exe.'" >dut_aps_and_scripts.sh');
		$telnet->command('echo "drvhlpr_app='.$drvhlpr_path.'" >dut_aps_and_scripts.sh');
		$telnet->command('echo "dut_file_saver_file='.$dut_path.'" >>dut_aps_and_scripts.sh');
		print "dut_aps_and_scripts.sh is ready\n";
	}
	#get scripts to run for dut mod:
	my $dut_stop = GetHost()->{"scripts_path_stop"};
	print "dut_stop is $dut_stop\n";
	my $dut_start = GetHost()->{"scripts_path_start"};
	print "dut_start is $dut_start\n";
		
	if(! $dut_stop)
	{
		print "NO PLATFORM FOUND !\n";
		sleep 2;
		exit(1);
	}

	 $telnet->command("$dut_stop");
	 $telnet->command("$dut_start");
	 $telnet->command('killall dutserver');
	 $telnet->command('cd /tmp');
	 $telnet->command('echo "'.$exe.' &" >run_dut.sh');
	 $telnet->command("chmod +x run_dut.sh");
	 $telnet->print("/bin/sh run_dut.sh");
	 print("DUT started\n");
     sleep(1);

}

sub StartTftpCalFile
{
    my $calibration_file = shift;
    my $target_ip = shift;
	my $exe;

	print("StartTftpCalFile: MyIP=".$MyIP."\n");
	if ($calibration_file eq "both")
	{
		#print("StartTftpCalFile: eq both\n");
		$exe = "cd /tmp \ntftp -gr cal_wlan0.bin ".$MyIP."\ntftp -gr cal_wlan1.bin ".$MyIP."\nchmod 644 cal_wlan*.bin";
	}
	else
	{
		#print("StartTftpCalFile: NOT eq both\n");
		$exe = "cd /tmp \ntftp -gr ".$calibration_file." ".$MyIP."\nchmod 644 cal_wlan*.bin";
	}		
	$telnet->command('echo "'.$exe.' " >/tmp/run_tftp_file.sh');
	$telnet->command("chmod +x /tmp/run_tftp_file.sh");
	$telnet->command("/bin/sh /tmp/run_tftp_file.sh");
#	print("run the command: \n");
#	print("".$exe."\n");
}

sub WaitForPing
{
	my ($host, $timeout) = @_;
	print("\nwaiting for ping to $host (timeout is $timeout)\n");
    my $p = Net::Ping->new();
	do {
		if ($timeout <=0)
		{
			$p->close();
			print("\nPing Timeout.\n");
			return 0;
		}
		print(".");
		$timeout=$timeout-1;
	} while ($p->ping($host,1)!=1);
  	print("\nPing Is OK.\n");

    $p->close();
	return 1;
}

sub SendReboot
{
	Colors::PrintRichtext("<white>Rebooting linux.</>\n");
	$telnet->print("reboot\n");
	Colors::PrintRichtext("<white>Please wait while the system is rebooting...</>\n");
	my $time =  time();
	sleep(10); #dont start with the ping before we sure the reboot started ...
	$telnet->close();
	my $ret = WaitForPing($TargetIP,180);
	my $sec = time() - $time;
	print("Total reboot time is $sec\n");
	return $ret;
}

sub GetHost
{
  return $telnet->GetHost();
}

sub ExtractGZIP
{
  my ($gzFile, $remoteDir) = @_;
  $telnet->command("cd ".$remoteDir);
  print('Extracting '.$gzFile."...\n");
  $telnet->command($telnet->GetHost()->{"unpack_cmnd"});
  $telnet->command('rm -f '.$telnet->GetHost()->{"images_file"});
}

sub MoveProgmodels
{
  my ($image_dir, $progmodel_dir) = @_;
  if ($image_dir ne $progmodel_dir)
  {
      print("moving any progmodels...\n");
      $telnet->command("cd ".$image_dir);
      $telnet->command("mv ProgModel* ".$progmodel_dir);
  }
}

sub LinkFirmware
{
  my ($image_dir, $progmodel_dir) = @_;
  if ($image_dir ne $progmodel_dir)
  {
      print("linking firmware...\n");
      $telnet->command("cd ".$progmodel_dir);
      $telnet->command("ln -s ".$image_dir."/contr_um.bin .");
      $telnet->command("ln -s ".$image_dir."/contr_lm.bin .");
  }
}

sub GetHWId
{
  my $ret = $telnet->command("lspci -d 1a30:");

  #if no lspci try direct access
  if ($ret !~ /.*1a30\:?(\d{4}).*$/)
  {
    $ret = $telnet->command("cat /proc/pci | grep 1a30");
  }

  if ($ret !~ /.*1a30\:?(\d{4}).*$/)
  {
    $ret = $telnet->command("cat /proc/bus/pci/devices | grep 1a30");
  }

  if ($ret =~ /.*1a30\:?(\d{4}).*$/)
  {
     return $1;
  }

  return ""
}

sub GetKernelVersion
{
  my $ret = $telnet->command("uname -r");
  return $ret
}

1;

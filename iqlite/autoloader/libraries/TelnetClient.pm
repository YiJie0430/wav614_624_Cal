use strict;
use Colors;
package TelnetClient;
use Net::Telnet;
use Cwd;
use vars qw(@ISA);
@ISA = qw(Net::Telnet); # Inherits from telnet

my $targetIP          = '192.168.1.1';
my $curHost = {}; # A structure containing information about the host (dongle / star / pirelli / etc...)

my @telnet_command_history=();

my $current_path = 'dummy';
my $current_app = 'dummy';

sub new {
    my $type = shift;
    my $self = Net::Telnet->new;
    return bless $self, $type;
}

sub print_history {
    print("\n-------------------------------------------------------------\n");
    print(@telnet_command_history);
    print("\n-------------------------------------------------------------\n");
}

################################
##   VersionAwareLogin
##   This function is partially copied from login() of Telnet.pm
##   Login to host, while detecting its version and filling $curHost properly
################################
sub VersionAwareLogin {
    my $self = shift;
    my (
	$errmode,
	$error,
	$lastline,
	$match,
	$ors,
	$prematch,
	$prompt,
	$s,
	$timeout,
	);
    local $_;

    ## Init.
    $self->timed_out('');
    $self->last_prompt("");
    $s = *$self->{net_telnet};
    $timeout = $self->timeout;
    $ors = $self->output_record_separator;
    $prompt = $self->prompt;
    
   ## Override some user settings.
    local $s->{errormode} = $errmode
	if defined $errmode;
    local $s->{time_out} = $self->_endtime($timeout);

    ## Create a subroutine to generate an error.
    $error
	= sub {
	    my ($errmsg) = @_;

	    if ($self->timed_out) {
		return $self->error($errmsg);
	    }
	    elsif ($self->eof) {
		($lastline = $self->lastline) =~ s/\n+//;
		return $self->error($errmsg, ": ", $lastline);
	    }
	    else {
		return $self->error($self->errmsg);
	    }
	};

	return $self->error("login failed: filehandle isn't open")
	if $self->eof;

	## Read linux_hosts.txt configuration file
        open (CFG_FILE, "linux_hosts.txt") || die('Cannot find linux_hosts.txt file in:' . getcwd);
	my @hosts = ();
	$curHost = {};
	while (<CFG_FILE>)
	{
		if (/^\/\//) {}
		elsif (/^\[(.+)\]/)
		{
			if ($curHost->{"name"}) { push @hosts, $curHost };
			$curHost = {};
			$curHost->{"name"} = $1;
		}
		elsif (/^(\S*)\s*=\s*(.*)/) { $curHost->{$1} = $2; }
#		print("Added curHost-".$1."=".$2."\n");
	}
	if ($curHost->{"name"}) { push @hosts, $curHost; }
    close( CFG_FILE );
	
	my @matchArr = ();
	for (my $i=0; $i<scalar(@hosts); $i++)
	{
		my $tmpMatch = $hosts[$i]->{"login_match"};
		push @matchArr, Match => "/".$tmpMatch."/i";
	}
	
    ## Wait for login prompt.
#	push @matchArr, Match => '/Dorango lodgin:/i';
#	push @matchArr, Match => '/login[: ]*$/i';
	
    ($prematch, $match) = $self->waitfor(@matchArr, 
		   Errmode => "return")
	or do {
	    return &$error("eof read waiting for login prompt")
		if $self->eof;
	    return &$error("timed-out waiting for login prompt");
	};

	$curHost = {};
	$curHost->{"firstFlag"} = "1";
	my $severalMatches = 0;
	my $tmpLoginMatch = "";
	for( my $i = 0; $i < scalar(@hosts); $i++ ) {
		if ($match =~ m/($hosts[$i]->{"login_match"})/i )
		{
                        if ($curHost->{"firstFlag"} eq "1")
                        {
	                    $curHost = {};
						$curHost->{"firstFlag"} = "0";
			$curHost = $hosts[$i];
                        }
                        else 
                        {
                            $severalMatches = 1;
                            $tmpLoginMatch = $match;
                        }
			#last;
		}
	}
	print("severalMatches= ".$severalMatches."\n");
	if (!$curHost->{"name"}) { die('Cannot find match host'); } # Should never reach
	if ( $severalMatches == 1)
        {
            print("Found several possible matches for target\n");
        }
        else 
        { 
	Colors::PrintRichtext("Detected host: <yellow>".$curHost->{"name"}."</>\n");
        }

    ## Delay sending response because of bug in Linux login program.
#    $self->_sleep(0.01);

	if ($curHost->{"username"})
	{
		print("Sending login username...\n");
		## Send login name.
		$self->put(String => $curHost->{"username"} . $ors,
	       Errmode => "return")
	   or return &$error("login disconnected");

		if ($curHost->{"password"})
		{
		    ## Wait for password prompt.
		    $self->waitfor(Match => '/password[: ]*$/i',
				   Errmode => "return")
		  	or do 
			{
			    return &$error("eof read waiting for password prompt") if $self->eof;
		  	    return &$error("timed-out waiting for password prompt");
		    }
		}
	}
	
	if ($curHost->{"password"})
	{

 	   print("Sending login password...\n");
	    ## Send password.
		$self->put(String => $curHost->{"password"} . $ors, Errmode => "return")
		or return &$error("login disconnected");
	};

	    ## Delay sending response because of bug in Linux login program.
#    $self->_sleep(0.01);

	if ($curHost->{"password"} || $curHost->{"username"})
	{
	    ## Wait for command prompt or another login prompt.
	    ($prematch, $match) = $self->waitfor(Match => '/login[: ]*$/i',
						 Match => '/username[: ]*$/i',
						 Match => $prompt,
						 Errmode => "return")
		or do {
		    return &$error("eof read waiting for command prompt")
			if $self->eof;
		    return &$error("timed-out waiting for command prompt");
		};
		## It's a bad login if we got another login prompt.
		return $self->error("login failed: bad name or password")
		if $match =~ /login[: ]*$/i or $match =~ /username[: ]*$/i;
	}

        print("checking system type for match >>>".$tmpLoginMatch."<<<\n");
        
        my $commandstring = "cat /proc/cpuinfo | grep system | awk '{print \$4}'";
        my $ret = '';
        $match = '';
        ($ret,$match) = $self->cmd(String=>$commandstring,Timeout=>200);

        $ret =~ s/\n//;
        $ret =~ s/\r//;

        #my $ret = cmd("cat /proc/cpuinfo | grep system | awk print $4");
        print("cpuinfo returned >>>".$ret."<<<\n");
        my $curSystem = $ret;
        if ( $severalMatches == 1)
        {
            for( my $i = 0; $i < scalar(@hosts); $i++ ) {
                print("checking system type login ".$hosts[$i]->{"login_match"}."\n");
    		if( $hosts[$i]->{"login_match"} eq $tmpLoginMatch )
    		{
                       print("checking ".$hosts[$i]->{"name"}." with system type >>>".$hosts[$i]->{"system_type"}."<<<\n");
                       if( $hosts[$i]->{"system_type"} eq $curSystem )
                       {
                           print("found ".$hosts[$i]->{"system_type"}."\n");
                           $curHost = $hosts[$i];
                           Colors::PrintRichtext("Detected host: <yellow>".$curHost->{"name"}."</>\n");
                           last;
                       }
    		}
      	    }
        }

    ## Save the most recently matched command prompt.
    $self->last_prompt($match);
	print("Using system: ".$curHost->{"name"}."\n");
	# Do post-login commands if needed
	if ($curHost->{"detect_module"})
	{
		require $curHost->{"detect_module"};
		$curHost->{"name"} = DetectBoard($self);
	}
	
	# Find the application on board (loop until find location or end of paths in conf file)
	for (my $i=1; $i<100; $i++)
	{
		print("search for apps\n");
		print("loop = ".$i."\n");
		if ($curHost->{"dut_exe$i"} || $curHost->{"dut_exe"})
		{
			print("found next path\n");
			print("dut_exe path=".$curHost->{"dut_exe"}."\n");
			if ($curHost->{"dut_exe"} && $i == 1) {
				# Legacy - only one path exist
				print("Legacy\n");
				$current_app = $curHost->{"dut_exe"};
			} else {
				print("Non-Legacy, path=".$curHost->{"dut_exe$i"}."\n");
				if ($curHost->{"dut_exe$i"}) {
					$current_app = $curHost->{"dut_exe$i"};
				}
				else {
					#end of list, no file found. Error:
					print "Error - could not find dutserver\n";
					exit (1);
				}				
			}
			print "current_app = $current_app\n";
			
			my $commandstring_aps = 'if test -f "'.$current_app.'"; then echo "Success"; else echo "Fail";fi';
			
			$current_path = "/tmp";
			print "commandstring_aps = $commandstring_aps\n";
			
			#Send command (via telnet->print) and wait response.
			# If not found, return error contain: "ls...: No such file or directory".
			# We want to keep "No such", therefore match has to point word after:
			$self->buffer_empty;
		    $self->print($commandstring_aps);	
	
			$ret = $self->getline();
			$ret =~ s/\n//; 
			while(($ret ne "Success") and ($ret ne "Fail"))
			{
				$ret = $self->getline();
				$ret =~ s/\n//;
			}
				
			print("ret = ".$ret."\n");	
			if ($ret eq "Success")
			{
				print("Application found: $current_app\n");
				$curHost->{"dut_exe"} = "$current_app";
				# drvhlpr
				$current_app =~ s/dutserver/drvhlpr/ig;
				$curHost->{"drvhlpr_app"} = "$current_app";
				
				#break loop:
				last
			}
		}
		else
		{
			print "ERROR - could not find dutserver\n";
			sleep(2);
			exit(1);
		}
	}
	
	for (my $i=1; $i<100; $i++)
	{
		print("search for scripts\n");
		print("loop = ".$i."\n");

		#Send command (via telnet->print) and wait response.
		# If not found, return error contain: "ls...: No such file or directory".
		# We want to keep "No such", therefore match has to point word after.

		if ($curHost->{"path$i"})
		{
			$current_path = $curHost->{"path$i"};
			#print "current_path = $current_path\n";

			#Send first script command:			
			$self->buffer_empty;
		    my $commandstring1 = 'if test -f "'.$current_path.'/fapi_wlan_wave_dut_drvctrl.sh"; then echo "Success"; else echo "Fail";fi';
			print("exec commandstring1 = ".$commandstring1."\n");
			$self->print($commandstring1);			
			$ret = $self->getline();
			$ret =~ s/\n//; 
			while(($ret ne "Success") and ($ret ne "Fail"))
			{
				$ret = $self->getline();
				$ret =~ s/\n//;
			}
				
			print("ret = ".$ret."\n");	
			if ($ret eq "Success")
			{
				print("Path and File name found: $current_path and fapi_wave_wlan_...\n");
				$curHost->{"scripts_path"} = "$current_path/fapi_wlan_wave_dut_file_saver.sh";
				$curHost->{"scripts_path_stop"} = "$current_path/fapi_wlan_wave_dut_drvctrl.sh stop";
				$curHost->{"scripts_path_start"} = "$current_path/fapi_wlan_wave_dut_drvctrl.sh start";
				$curHost->{"scripts_path_stop_helper"} = "$current_path/fapi_wlan_wave_dut_drvctrl.sh stop_helper";
				$curHost->{"scripts_path_start_helper"} = "$current_path/fapi_wlan_wave_dut_drvctrl.sh start_helper";
				#break loop:
				last
			}
			
			#Send second script command:
			$self->buffer_empty;
			
			my $commandstring2 = 'if test -f "'.$current_path.'/wave_wlan_dut_drvctrl.sh"; then echo "Success"; else echo "Fail";fi';
			print("exec commandstring2 = ".$commandstring2."\n");
			$self->print($commandstring2);			
			$ret = $self->getline();
			$ret =~ s/\n//; 
			while(($ret ne "Success") and ($ret ne "Fail"))
			{
				$ret = $self->getline();
				$ret =~ s/\n//;
			}
				
			print("ret = ".$ret."\n");	
			if ($ret eq "Success")
			{
				print("Path and File name found: $current_path and wave_wlan_...\n");
				$curHost->{"scripts_path"} = "$current_path/wave_wlan_dut_file_saver.sh";
				$curHost->{"scripts_path_stop"} = "$current_path/wave_wlan_dut_drvctrl.sh stop";
				$curHost->{"scripts_path_start"} = "$current_path/wave_wlan_dut_drvctrl.sh start";
				$curHost->{"scripts_path_stop_helper"} = "$current_path/wave_wlan_dut_drvctrl.sh stop_helper";
				$curHost->{"scripts_path_start_helper"} = "$current_path/wave_wlan_dut_drvctrl.sh start_helper";			
				#break loop:
				last
			}
		}
		else
		{
			#End of paths, break loop:
			print("ERROR: End of paths, no DUT script found, exit\n");
			sleep(2);
			exit(1);
		}
	} #loop
	
    1;
} # end sub login

sub Connect
{
    my $self = shift;
    my $target_ip = shift;
    $targetIP = $target_ip; # remeber the target IP
    $self->open($target_ip);

    print "opened telnet to $target_ip\n";
    my $ret = $self->VersionAwareLogin();
    @telnet_command_history = ();
	return $ret;
}

sub command
{
    my $self = shift;
    my $cmd_str = shift;
    #print ">> $cmd_str\n";
    my $pre = '';
    my $match = '';
    ($pre,$match) = $self->cmd(String=>$cmd_str,Timeout=>200);
    #print ("$pre\n");
    push(@telnet_command_history, $cmd_str."\n");
    push(@telnet_command_history, $pre."\n");
    return $pre;
}

sub commandarray
{
    my $self = shift;
    my $cmd_str = shift;
    #print ">> $cmd_str\n";
    my @pre = ();
    @pre = $self->cmd(String=>$cmd_str,Timeout=>200);
    #print ("$pre\n");
#    push(@telnet_command_history, $cmd_str."\n");
#    push(@telnet_command_history, $pre."\n");
    return @pre;
}

# Returns the host structure
sub GetHost
{
  my $self = shift;
  return $curHost;
}

1;
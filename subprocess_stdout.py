import subprocess
import threading
import Queue
import logging_local
import logging_local.config
import time
logging_local.config.fileConfig('logging.ini')

logger = logging_local.getLogger(__name__)


def task_read_stdout(process, queue):
    """Read from stdout"""
    for output in iter(process.stdout.readline, b''):
        queue.put(output)
    
    #upper's statment equals below's one
    ''' 
    while 1:
        output = process.stdout.readline()
        if output != '':
            queue.put(output)
        else:
            break
    '''
    return 

def task_read_stdout(process, queue):
    """Read from stdout"""
    while True:
        output = process.stdout.read(70)
        if not output and process.poll() is not None:
            process.stdout.flush()
            break
        else:
            if not output:
                logger.info('no output')
                time.sleep(0.1)
            else:
                queue.put(output)
                time.sleep(0.01)
    return 


def main():

    process = subprocess.Popen('test.bat',
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT,
                               bufsize=1,
                               shell=True
                               )

    queue = Queue.Queue()
    task = threading.Thread(target=task_read_stdout, args=(process, queue))
    task.daemon = True
    task.start()

    while process.poll() is None or not queue.empty():
        try:
            output = queue.get(timeout=1.5)
        except Queue.Empty:
            print 'queue is empty'
            process.terminate()
            process.wait()    
            continue

        if not output:
            continue

        print(output)
            
    print process.pid

    task.join()

if __name__ == '__main__':
    main()